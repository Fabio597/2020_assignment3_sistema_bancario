FROM maven:3.6-jdk-8-slim

WORKDIR /banks_system
COPY . /banks_system
RUN mvn clean
CMD ["mvn", "spring-boot:run"]