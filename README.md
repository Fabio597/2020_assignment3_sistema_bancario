# Gestione Banche (Assignment 3)

## Sommario
1. Descrizione Progetto
2. Installazione e Run del Progetto
3. Database
4. Pattern MVC 
5. Contributors

### Descrizione Progetto
Questo progetto è stato creato per lo sviluppo del terzo assignment del corso di Processo e Sviluppo del Software.
Lo scopo della web application è quello di gestire alcuni dati riguardanti un gruppo di banche, eseguendo tutte le operazioni CRUD(Create, Read, Update e Delete) e alcune operazioni di ricerca sui dati. In questo modo possiamo _simulare_ con la gestione dei dati una sorta di super utente/amministratore del sistema (Sopra è stato specificato _simulare_ perchè non verrà implementata la gestione dell'autenticazione). Il database che verrà utilizzato è MySQL e questo attraverso la containerizzazione dell'applicazione, quindi avremo due container che comunicheranno: Il container per la __web application in Spring Boot__ e il container di __MySQL__.
Per questo progetto viene creata una applicazione __Spring Boot__ completa in modo da salvare i dati in modo persistente e di poterli visualizzare in modo dinamica attraverso alcune pagine Web. Inoltre per gestire le dipendenze del progetto utilizzeremo __Maven__, mentre per il design delle pagine web abbiamo utilizzato il framework __Bootstrap__.

__N.B.:__Specifichiamo anche che non sono stati implementati controlli nell'aggiunta dei dati all'interno delle form, ovvero se è necessario inserire ad esempio: età, saldo, numero ufficio o numero sportello sarà presente una casella di testo in cui è necessario inserire un valore numerico intero.

### Installazione e Run del Progetto
Per installare ed eseguire la web application è necessario scaricare il codice dalla Repository ed avere già installato sul proprio dispositivo __Docker Desktop__ oppure la __Docker Toolbox__.
Dopo aver clonato la Repository, è necessario posizionarsi con il _prompt dei comandi_ o il _terminale_ nella root del progetto, dove è presente il file __docker-compose.yml__. A questo punto è solo necessario eseguire l'istruzione:
```
docker-compose up
```
Ua volta inizializzato il progetto, quindi dopo che i container sono istanziati e il container della Spring Boot WebApp ha terminato di eseguire il clean iniziale delle dipendenze dato dal comando: __mv clean__, sarà possibile visualizzare la pagina principale della WebApp al link: __localhost:8080__ (se si ha installato _Docker Desktop_), oppure è necessario specificare l'indirizzo IP della macchina virtuale su cui gira Docker: __IP_VIRTUAL_MACHINE:8080__ (se si ha installato la _Docker Toolbox_).

### Organizzazione Progetto
All'interno del progetto abbiamo collocato le classi __java__ all'interno di package per rendere più ordinato e coerente il progetto. I package presenti sono:
* __com.assignment.sistemabancario__: In questo package è presente il metodo _main_ del progetto, quindi l'entry point della web app.
* __com.assignment.sistemabancario.controllers__: In questo package sono presenti due _View Controller_ (WelcomViewController e ManageDataViewController) che gestiscono la pagina principale (Home) della Web application e la pagina che permette di scegliere quali dati gestire. 
* __com.assignment.sistemabancario.controllers.manage_data__: In questo package sono presenti i _View Controller_ che gestiscono ogni operazione CRUD, eccetto la Update, relativi a tutte le entità. Le classi sono rispettivamente: _ManageBancaViewController, ManageClienteViewController, ManageContoCorrenteViewController, ManageLavoratoreViewController e ManageNazioneViewController_.
* __com.assignment.sistemabancario.controllers.manage_data.updates__: In questo package sono presenti i _View Controller_ che gestiscono l'operazione di Update, relativi a tutte le entità. Abbiamo deciso di separare l'Update per rendere più semplice lo sviluppo della Web App e garantire una User Experience migliore. Le classi sono rispettivamente: _ModificaBancaViewController, ModificaClienteViewController, ModificaContoCorrenteViewController, ModificaLavoratoreViewController e ModificaNazioneViewController_.
* __com.assignment.sistemabancario.model__: In questo package sono contenute le classi del modello che non descrivono oggetti che saranno persistenti, ma solo quegli oggetti che vegono utilizzate nella logica di Businness, in modo da separare le due logiche. Le classi sono: _ContoLuxury_(Che memorizza una Banca, un Conto e un insieme di Clienti, permette di facilitare la query di search per ottenere i conti correnti più ricchi) e _DBOperations_(Permette di eseguire le query e le operazioni di search).
* __com.assignment.sistemabancario.model.db__: In questo package sono presenti le classi che mappano i dati persistenti. In sostanza le classi che attraverso le annotazioni rappresentano le entità e le relazioni tra esse. Le classi sono: _Banca, Cliente, ContoCorrente, Direttore, Impiegato, Lavoratore e Nazione_
* __com.assignment.sistemabancario.repository__: In questo package sono contenute le repository, quindi le classi che estendono __JPARepository<T,ID>__. Questi oggetti saranno poi in grado di effettuare concretamente le operazioni CRUD all'interno della Base Dati. Le classi sono: _BancaRepository, ClienteRepository, ContoCorrenteRepository, DirettoreRepository, ImpiegatoRepository, LavoratoreRepository e NazioneRepository_.

Abbiamo collocato anche i file __jsp__ all'interno di cartelle per riuscire a distinguere meglio lo scopo dei file stessi:
* __src/main/webapp/WEB-INF/jsp/__: Sono presenti i due file welcome.jsp e manage_data.jsp.
*  __src/main/webapp/WEB-INF/jsp/manage_data_pages__: Pagine jsp che gestiscono le operazioni di create, read e delete di ogni informazione presente nel database. 
*  __src/main/webapp/WEB-INF/jsp/manage_data_pages/modify_data_pages__: Pagine jsp che gestiscono l'operazione di update' di ogni informazione presente nel database. 


### Database
Per la gestione del Database utilizziamo un'interfaccia applicativa per costruire il nostro Database attraverso un sistema __ORM__(Object Relational Mapping). Questa interfaccia che utilizziamo si chiama__ JPA(Java Persistence API)__ che permette di mappare le tabelle del Database relazionale con delle classi Java, e di specificare le relazioni tra esse attraverso Annotazioni e attributi delle classi.
Per ogni oggetto persistente del database creiamo un oggetto _Repository_ che estende l'interfaccia __JPARepository<T, ID>__ e ci permette di eseguire le operazioni CRUD all'interno della base dati.
Il Database della nostra WebApp è strutturato come segue:
![image info](./Images_readme/ER-Database.png)


Specifichiamo inoltre che per ogni entità la chiave primaria (_dato di tipo long_) viene autogenerata incrementando un valore intero ogni volta che viene creato un nuovo oggetto da persistere. Questo viene fatto utilizzado l'annotazione:
```java
@GeneratedValue(strategy = GenerationType.IDENTITY)
```
Daremo anche una breve descrizione delle entità in gioco e delle relazioni tra esse.

#### Entità
* __Nazione__: L'entità Nazione permette di salvare i record relativi sia alla Nazionalità della Banca, sia quella del Cliente. Sarà un dato che verrà visualizzato insieme alla descrizione della Banca e del Cliente. In questa entità salviamo solo l'id della nazione e il nome.
* __Banca__: L'entità Banca permette di salvare i dati relativi alla Banca in generale, non necessariamente di una filiale. In questa entità salviamo l'id (per ogni singolo record), il nome della Banca e la nazione (in relazione).
* __Lavoratore__: L'entità Lavoratore permette di memorizzare i dati relativi ai dipendenti della banca. In questa entità salviamo id, nome, cognome, età del dipendente e infine la Banca per cui lavora (relazione con l'entità Banca).
* __Impiegato__: Questa entità è una specializzazione del Lavoratore, perchè risulta essere sempre un lavoratore ma un po più specializzato. In questo caso essedo un Impiegato inseriamo come attributo solo per l'impiegato il _numero dello sportello_.
* __Direttore__: Questa entità, analogamente all'impiegato, risulta essere una specializzazione dell'entità Lavoratore, e in questo caso un attributo che caratterizza solo il Direttore è il _numero del suo ufficio_.
* __ContoCorrente__: Questa entità permette di memorizzare i Conti Correnti aperti all'interno di una specifica Banca. I dati che salviamo all'interno di questa Entità sono: idContoCorrente, saldo attuale, la banca in cui è stato aperto il conto e i clienti intestatari del conto (accenniamo solo perchè spiegheremo bene sotto quando parleremo delle relazioni tra le entità).
* __Cliente__: Questa entità permette di memorizzare i Clienti di una Banca e metterli in relazione con il conto corrente che hanno aperto. Se un cliente non ha aperto un Conto Corrente può lo stesso essere presente nel Database perchè potrebbe aver chiuso un Conto ed è in attesa di aprirne uno nuovo. Per questa entità salviamo idCliente, nome, cognome, età, coniuge (relazione che spigheremo in seguito) e i conti correnti (come descritto per il conto corrente, spiegheremo bene sotto quando parleremo delle relazioni tra le entità)

#### Relazioni tra le Entità
Di seguito verranno spiegate e mostrate le relazioni tra le classi che rappresentano gli oggetti persistibili all'interno della base dati. Ognuna di queste classi è mappata sopra l'intestazione della classe con l'annotazione: __@Entity__ e __@Table(name = "Nome Tabella")__.
Ogni relazione è stata sviluppata in modo __Bidirezionale__ in modo da rendere più semplice e soprattutto efficiente la navigazione tra le entità.

* __Nazione-Banca (one-to-many)__: Questa relazione tra le due entità _Nazione_ e _Banca_ è stata creata perchè ragionevolmente una Banca ha una sola Nazione associata, ma una Nazione ha più di una Banca associata. Questa relazione attraverso, le __JPA__, è stata mappata con l'annotazione:
_Classe Banca_:
```java
@ManyToOne
@JoinColumn(name = "nazioneId")
private Nazione nazione;
```
All'interno della classe che mappa la relazione _Banca_ avremo un attributo singolo per la nazione, mentre all'interno della relazione _Nazione_ avremo una lista di Banche come attributo nella classe:
_Classe Nazione_:

```java
@OneToMany(mappedBy = "nazione")
private List<Banca> banche = new ArrayList<Banca>();
```
* __Nazione-Cliente (one-to-many)__: Questa relazione tra le due entità _Nazione_ e _Cliente_ è stata creata perchè ragionevolmente un Cliente ha una sola Nazione associata, ma una Nazione ha più Clienti associati. Per mappare questa relazione è stato creato un attributo nazione all'interno della classe Cliente: 
```java
@ManyToOne
@JoinColumn(name = "nazioneId")
private Nazione nazione;
```
Mentre nella classe Nazione è stato mappato con una lista di clienti:
```java
@OneToMany(mappedBy = "nazione")
private List<Cliente> clienti = new ArrayList<Cliente>();
```
* __Banca-ContoCorrente (one-to-many)__: Questa relazione tra le entità _Banca_ e _ContoCorrente_ rappresenta il fatto che nella realtà un conto corrente è associato ad una sola Banca, ma una Banca ha molti conti correnti aperti. Per mappare questa relazione è stato creato un attributo nella classe ContoCorrente e annotato come segue: 
```java
@ManyToOne
@JoinColumn(name = "bancaId")
private Banca banca;
```
Mentre nella classe Banca è stato mappato con un attributo lista (di ContiCorrenti):
```java
@OneToMany(mappedBy = "banca")
private List<ContoCorrente> contiCorrenti = new ArrayList<ContoCorrente>();
```
* __Cliente-ContoCorrente (many-to-many)__: Questa relazione tra le entità _Cliente_ e _ContoCorrente_ raprresenta il fatto che è possibile avere più conti correnti aperti in una o più banche e che un Conto Corrente sia stato intestato a più di un Cliente. Per mappare questa relazione, all'interno della classe Cliente è stato creato un attributo di tipo lista annotato come segue:
```java
@ManyToMany(mappedBy = "clienti")
Set<ContoCorrente> contiCorrenti = new HashSet<ContoCorrente>();
```
Mentre nella classe ContoCorrente è presente un attributo sempre di tipo lista (essendo la relazione many-to-many) come di seguito:
```java
@ManyToMany
@JoinTable(
name = "clienti_conti", 
joinColumns = @JoinColumn(name = "contoCorrente_id"), 
inverseJoinColumns = @JoinColumn(name = "cliente_id"))
Set<Cliente> clienti = new HashSet<Cliente>();
```
In questa ultima relazione specifichiamo anche come sono legate le due relazioni attraverso la notazione _@JoinColumn_.
* __Cliente-Cliente (self-loop one-to-one)__: Questa relazione tra l'entità _Cliente_ stessa, e rinominata __coniuge__, è stata creata per associare ad ogni cliente un altro record della tabella cliente che rappresenta il Coniuge di quella persona. Il valore può essere anche _null_ dato che non per forza una persona è sposata. La relazione è stata mappata con un attributo _coniuge_ annotato come di seguito:
```java
@OneToOne(cascade = CascadeType.ALL)
@JoinColumn(name="coniuge_id")
private Cliente coniuge;
```
* __Banca-Lavoratore (one-to-many)__: Questa relazione tra le entità _Banca_ e _Lavoratore_ è stata creata per rappresentare il fatto che per una Banca lavorano più persone (ci sono più lavoratori), ma un lavoratore può lavorare solo per una Banca. La mappatura all'interno della classe _Lavoratore_ è stata creata inserendo l'attributo relativo alla Banca e annotata come segue:
```java
@ManyToOne
@JoinColumn(name = "bancaId")
private Banca banca;
```
Mentre all'interno della classe _Banca_ è stata inserita una lista di lavoratori come attributo ed è stata annotata come segue:
```java
@OneToMany(mappedBy = "banca")
private List<Lavoratore> lavoratori = new ArrayList<Lavoratore>();
```
* __Gerarchia (Lavoratore-Impiegato-Direttore)__: Questa gerarchia tra Lavoratore-Impiegato e Lavoratore-Direttore è stata creata per rappresentare il fatto che _Impiegato_ e _Direttore_ sono due Lavoratori specializzati, ovvero hanno le stesse caratteristiche di un Lavoratore e qualche proprietà in più. La gerarchia verrà poi rappresentata concretamente con una singola tabella, dato che i record effettivi saranno relativi solo agli Impiegati e ai Direttori. Per effettuare la distinzione all'interno della tabella viene utilizzata una colonna detta _discrimination value_, chiamata __tipo_lavoratore__, che differenzia i Direttori con valore __0__ dagli Impiegati con valore __1__:
_Lavoratore_:
```java
@Entity
@Table(name = "lavoratori")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo_lavoratore", 
discriminatorType = DiscriminatorType.INTEGER)
public class Lavoratore { ...
```
_Impiegato_:
```java
@Entity
@DiscriminatorValue("1")
public class Impiegato extends Lavoratore { ...
```
_Direttore_:
```java
@Entity
@DiscriminatorValue("0")
public class Direttore extends Lavoratore {
```
__N.B.__: Facciamo notare che sono state separate le entità dei Lavoratori e dei Clienti per avere i dati separati. Abbiamo così preferito tenere la logica dei dati delle persone che lavorano all'interno della banca dai clienti completamente separata.

#### Eliminazione di Record
Tra le operazioni CRUD che andiamo a fare, mostriamo i metodi che utilizziamo e spiegheremo un po più in dettaglio l'eliminazione, essendo l'operazione leggermente più complessa da gestire.
1. __Create__: Utilizziamo il metodo _save(T t)_ richiamabile dall'oggetto Repository che estende _JpaRepository<T, ID>_.
2. __Read__: Utilizziamo il metodo _findById(ID id)_ per ottenere un singolo elemento, oppure _findAll()_, richiamabile dall'oggetto Repository che estende _JpaRepository<T, ID>_.
3. __Update__: Per eseguire l'update, otteniamo prima di tutto l'oggetto già esistente tramite il metodo descritto sopra e dopo aver effettuato le modifiche utilizzando i metodi __set__ presenti all'interno della classe, tramite il metodo _save(T t)_ andiamo a modificare l'oggetto nel Database.
4. Utilizziamo il metodo _deleteById(ID id)_ richiamabile dall'oggetto Repository che estende _JpaRepository<T, ID>_ per eliminare un singolo elemento.

Per eliminare un elemento e in alcuni casi impostare a _null_ il riferimento presente in un'altra tabella, utilizziamo le proprietà delle classi per muoverci all'interno delle relazioni e settare i valori.
Per fare questo abbiamo definito dei __metodi__ che prima di eliminare il record settano a _null_ i reference a se stessi presenti in altre classi, e questo viene fatto anteponendo al metodo l'annotazione _@PreRemove_
__Esempio:__
```java
@PreRemove
	public void setNullOnDelete() {
		this.contiCorrenti.forEach(contiCorrenti -> contiCorrenti.setBanca(null));
		this.lavoratori.forEach(lavoratore -> lavoratore.setBanca(null));
		if (this.nazione!= null) {
			this.nazione.getBanche().remove(this);
		}
	}
```
Sopra, prima di eliminare una Banca, settiamo il riferimento di quella Banca presente nei __Conti Correnti__, nei __Lavoratori__ e nella __Nazione__ a null.

#### Search e Query
All'interno della Web App sono state effettuate anche alcune query per ricercare dei dati sia attraverso le relazioni, __quindi navigando tra gli attributi delle classi che mappano le relazioni__, sia specificando la query __SQL__ all'interno dell'annotazione _@Query_:

1. __Conti con un certo Capitale__: Vengono prima presi tutti i conti presenti nella base dati e in seguito dopo aver selezionato solo i conti con il saldo attuale maggiore o uguale ad un certo valore, vengono recuperate le informazioni della banca e dei clienti che sono intestatari di quel conto, tutto questo grazie alle proprietà nella classe che mettono in realzione i dati presenti nel database.

2. __Banche con N Clienti__: In questo caso vengono prese prima tutte le banche presenti all'interno del database e per ognuno vengono contati tutti i clienti. Per fare questa operazione bisogna però prima passare dalla relazione tra Banca e ContoCorrente e successivamente arrivare a contare i clienti di quella Banca. Il valore relativo al numero di clienti lo salviamo all'interno di una proprietà della classe _Banca_ definita come _@Transient_ perchè non serve che sia presente all'interno del database.

```java
@Transient
private int numeroClienti = 0;
```

3. __Banche ordinate lessicograficamente__: In questo caso utilizziamo una query scritta direttamente in SQL e che specifichiamo con l'annotazione _@Query_:
```java
@Query(value = "select * from banca order by nome asc", nativeQuery = true)
public List<Banca> bancheOrdinatePerNome();
```
La query viene specificata all'interno della classe _Repository_ (In questo caso nella classe _BancaRepository_).
4. __Conti Correnti ordinati per Saldo__:  Ache in questo caso utilizziamo una query scritta direttamente in SQL e che specifichiamo con l'annotazione _@Query_:
```java
@Query(value = "select * from conti_correnti order by saldo desc", nativeQuery = true)
public List<ContoCorrente> contiOrdinatiPerSaldo();
```
La query viene specificata all'interno della classe _Repository_ (In questo caso nella classe _ContoCorrenteRepository_).

#### Accesso al Database
Per eseguire concretamente le query all'interno del database è possibile farlo da linea di comando accedendo al container mysql con il seguente comando:

```
docker exec -it mysql bash
```
Una volta eseguito il comando precedente ci si troverà all'interno del container mysql e bisognerà effettuare l'accesso al database per poter eseguire le query:

```
mysql -uroot -passignment3
```

Infine è nexcessario specificare il database da utilizzare attraverso il comando:

```
use banks_system_database;
```

Da questo punto in poi sarà possibile visualizzare le tabelle ed eseguire le query.

### Pattern MVC
Per la visualizzazione dei dati salvati all'interno del Database e per riuscire ad effettuare le operazioni __CRUD__ all'interno della Web Application, utilizziamo il pattern __Page Controller__, ovvero creiamo un _Controller_ per ogni pagina _jsp_ che viene visualizzata.
In questo modo ogni controller ha il compito di gestire le interazioni con l'utente attraverso _link_, _form_ e _bottoni_. Una volta interagito con il modello (per quanto riguarda la gestione dei dati) viene selezionata la view da mostrare (gestendo quindi il flow dell'applicazione).
In cima alla pagina principale è presente un bottone che permette di inserire alcuni dati di default: __Add Default Data__. Mentre per gestire i dati come un admin basta utilizzare il bottone: __Gestisci dati__.
Un esempio del flusso presente all'interno della nostra Web App è il seguente:
![image info](./Images_readme/esempio_mvc.png)

Per mappare le interazioni tra le pagine __jsp__ e i __View Controller__ nella web app, utilizziamo le form percchè accumuliamo i dati che devono essere inviati al controller. Di seguito mostriamo un esempio:

Nella pagina _jsp_ specifichiamo il metodo all'interno della Form (GET o POST)  e la action per indicare dove vogliamo inviare i dati.
```html
<form method="POST" action="/addBancaInDB">
  	<div class="form-group">
    	<label>Nome Banca</label>
    	<input type="text" class="form-control" name="nomeBanca" id="nomeBanca" placeholder="Aggiungi Nome Banca">
  	</div>
  	<div class="form-group">
    	<label>Seleziona Banca</label>
    	<select class="form-control" id="nazioneBanca" name="nazioneBanca">
      		<option value="0">Non Definito</option>
      		<c:forEach items="${nazioni}" var="nazione">
      		<option value="${nazione.getNazioneId()}">${nazione.getNome()}</option>
      		</c:forEach>
    	</select>
  	</div>
  	<button type="submit" class="btn btn-primary">Aggiungi Banca</button>
</form>
```
Per mappare l'azione all'interno del View Controller, utilizziamo l'annotazione: _@RequestMapping_ e specifichiamo lo stesso URL presente ell'actiondella form.
I parametri vengono passati attraverso l'intestazione del metodo specificando il nome presente nell'attributo del tag html _name_.
Come si può notare per mostrare i dati presenti all'interno del nostro modello utilizziamo la notazione: _${nazione.getNome()}_, questo perchè le pagine jsp sono dinmiche, quindi prima viene eseguito il codice java e poi viene inviata la risorsa (pagina HTML) al client.
Inoltre per eseguire i controlli abbiamo utilizzato la libreria __JSTL__:
```HTML
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
```

```html
<c:if></c:if>
<c:choose></c:choose>
<c:forEach></c:forEach>
```
Passando poi nel flusso al _View Controller_, all'interno del metodo vengono eseguite le operazioni per aggiornare il modello e infine viene selezionata la view seguente da mostrarre tramite l'oggetto __ModelAndView__.

```java
@RequestMapping(value = "/addBancaInDB", method = RequestMethod.POST)
public ModelAndView addBanca(int nazioneBanca, String nomeBanca) { ...
```
#### Struttura classi per operazioni nel DB e dipendency injection
All'interno del progetto abbaimo creato una classe che viene istanziata in ogni _View Controller_ che permette di accedere ad ogni repository per effettuare le operazioni CRUD  e definisce anche i metodi per richiamare le query di ricerca che abbiamo citato sopra.
Questa classe (__DBOperations__ viene definita con l'annotazione _@Service_ perchè grazie a Spring potremo poi __iniettare__ questa dipendenza dove la necessitiamo, in questo caso nei view controller). All'interno dei view controller basterà appunto iniettare la dipendenza attraverso l'annotazione __@Autowired__.

![image info](./Images_readme/uml_db.png)

Nel diagramma vengono rappresentate solo 3 repository, ma la classe DBOperations conterrà tutte le repository della web app.
Abbiamo strutturato così l'accesso al Database per comodità, perchè ci permette di avere un singolo oggetto che ci permette di accedere ad ogni repository e che esegue anche le ricerche su un set di entità più ampio.

### Contributors
Fabio D'Adda 817279 f.dadda4@campus.unimib.it  
Mario Enrico Centrone 816325 m.centrone2@campus.unimib.it  
Link alla Repository: https://gitlab.com/Fabio597/2020_assignment3_sistema_bancario