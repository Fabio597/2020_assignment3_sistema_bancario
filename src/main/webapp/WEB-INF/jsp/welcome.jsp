<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<meta charset="ISO-8859-1">
	<title>Sistema Gestione Banche</title>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
  			<a class="navbar-brand" href="#">Home</a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    			<span class="navbar-toggler-icon"></span>
  			</button>

  			<div class="collapse navbar-collapse" id="navbarSupportedContent">
    			<ul class="navbar-nav mr-auto">
      				<li class="nav-item active">
        				<a class="nav-link" href="/manageData">Gestisci dati<span class="sr-only">(current)</span></a>
      				</li>
      				<c:if test="${addDefaultDataVisible}">
      					<li class="nav-item">
        					<a class="nav-link" href="/addDefaultData">Add Default Data</a>
      					</li>
      				</c:if>
    			</ul>
  			</div>
		</nav>
		
		<div class="jumbotron">
    		<h1>Sistema di Gestione delle Banche</h1>      
    		<p>Introduzione al sistema di gestione delle banche.</p>
    		<p>Di seguito le banche salvate ordinate lessicograficamente per nome.</p>
  		</div>
  		
  		<table class="table">
  			<thead>
    			<tr>
      				<th scope="col">#</th>
      				<th scope="col">Banca</th>
      				<th scope="col">Nazione</th>
    			</tr>
  			</thead>
  			<tbody>
  				<c:forEach items="${banche}" var="banca">
    				<tr>
      					<th scope="row">${banca.getId()}</th>
      					<td>${banca.getName()}</td>
      					<c:choose>
    						<c:when test="${banca.getNazione()!=null}">
        						<td>${banca.getNazione().getNome()}</td>
    						</c:when>    
    						<c:otherwise>
        						<td>Non Definito</td>
    						</c:otherwise>
						</c:choose>
    				</tr>
    			</c:forEach>
  			</tbody>
		</table>
		<div class="jumbotron">
    		<h2>Conti con un certo capitale</h2>      
    		<p>Conti con importo maggiore di 5000</p>
  		</div>
		<table class="table">
  			<thead>
    			<tr>
      				<th scope="col">Banca</th>
      				<th scope="col">Saldo</th>
      				<th scope="col">Clienti</th>
    			</tr>
  			</thead>
  			<tbody>
  				<c:forEach items="${contiLuxury}" var="conto">
    				<tr>
    					<c:choose>
    						<c:when test="${conto.getBancaLuxury()!=null}">	
      							<td>${conto.getBancaLuxury().getName()}</td>
      						</c:when>
      						<c:otherwise>
        						<td>-</td>
    						</c:otherwise>
    					</c:choose>
      					<td>${conto.getContoLuxury().getSaldo()}$</td>
      					<c:choose>
      						<c:when test="${!conto.getClientiLuxury().isEmpty()}">
    							<td>
									<ul>
										<c:forEach items="${conto.getClientiLuxury()}" var="cliente">
											<li>${cliente.getNome()} ${cliente.getCognome()}</li>
										</c:forEach>
									</ul>
								</td>
    						</c:when>
    						<c:otherwise>
        						<td>-</td>
    						</c:otherwise>
    					</c:choose>
    				</tr>
    			</c:forEach>
  			</tbody>
		</table>
		<div class="jumbotron">
    		<h2>Conti Ordinati per capitale</h2>      
    		<p>Ordine per Saldo</p>
  		</div>
  		<table class="table">
  			<thead>
    			<tr>
      				<th scope="col">Saldo</th>
      				<th scope="col">Clienti</th>
    			</tr>
  			</thead>
  			<tbody>
  				<c:forEach items="${contiOrdinatiPerSaldo}" var="conto">
    				<tr>
      					<td>${conto.getSaldo()}$</td>
      					<c:choose>
      						<c:when test="${!conto.getClienti().isEmpty()}">
    							<td>
									<ul>
										<c:forEach items="${conto.getClienti()}" var="cliente">
											<li>${cliente.getNome()} ${cliente.getCognome()}</li>
										</c:forEach>
									</ul>
								</td>
    						</c:when>
    						<c:otherwise>
        						<td>-</td>
    						</c:otherwise>
    					</c:choose>
    				</tr>
    			</c:forEach>
  			</tbody>
		</table>
		<div class="jumbotron">
    		<h2>Banche con almeno 4 clienti</h2>      
    		<p>Banche con un certo numero di clienti</p>
  		</div>
  		<table class="table">
  			<thead>
    			<tr>
      				<th scope="col">Banca</th>
      				<th scope="col">Numero Clienti</th>
    			</tr>
  			</thead>
  			<tbody>
  				<c:forEach items="${bancheConNClienti}" var="banca">
    				<tr>
      					<td>${banca.getName()}</td>
      					<td>${banca.getNumeroClienti()}</td>
    				</tr>
    			</c:forEach>
  			</tbody>
		</table>
	</div>

	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>
</body>
</html>