<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Inserisci Cliente</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron">
    		<h1>Aggiungi Cliente</h1>      
    		<p>Arricchisci la base dati aggiungendo un Cliente o una Cliente!!.</p>
  		</div>
  		<div class="jumbotron">
  			<h2>Aggiungi Cliente</h2>
  			<form method="POST" action="/addClienteInDB">
  				<div class="form-group">
    				<label>Nome Cliente</label>
    				<input type="text" class="form-control" name="nomeCliente" id="nomeCliente" placeholder="Aggiungi Nome Cliente">
  				</div>
  				<div class="form-group">
    				<label>Cognome Cliente</label>
    				<input type="text" class="form-control" name="cognomeCliente" id="cognomeCliente" placeholder="Aggiungi Cognome Cliente">
  				</div>
  				<div class="form-group">
    				<label>Et� Cliente</label>
    				<input type="text" class="form-control" name="etaCliente" id="etaCliente" placeholder="Aggiungi Et� Cliente">
  				</div>
  				<div class="form-group">
    				<label for="exampleFormControlSelect1">Seleziona Nazione</label>
    				<select class="form-control" id="nazioneCliente" name="nazioneCliente">
      					<option value="0">Non Definito</option>
      					<c:forEach items="${nazioni}" var="nazione">
      						<option value="${nazione.getNazioneId()}">${nazione.getNome()}</option>
      					</c:forEach>
    				</select>
  				</div>
  				<div class="form-group">
    				<label for="exampleFormControlSelect1">Seleziona Coniuge</label>
    				<select class="form-control" id="coniugeCliente" name="coniugeCliente">
      					<option value="0">Non Definito</option>
      					<c:forEach items="${clienti}" var="cliente">
      						<option value="${cliente.getId()}">${cliente.getNome()} ${cliente.getCognome()}</option>
      					</c:forEach>
    				</select>
  				</div>
  				<button type="submit" class="btn btn-primary">Aggiungi Cliente</button>
			</form>
		</div>
		<div class="jumbotron">
			<h2>Clienti gi� presenti</h2>
			<table class="table">
  				<thead>
    				<tr>
      					<th scope="col">#</th>
      					<th scope="col">Nome</th>
      					<th scope="col">Cognome</th>
      					<th scope="col">Et�</th>
      					<th scope="col">Nazione</th>
      					<th scope="col">Coniuge</th>
      					<th scope="col">Conti Correnti Aperti</th>
    				</tr>
  				</thead>
  				<tbody>
  					<c:forEach items="${clienti}" var="cliente">
    					<tr>
      						<th scope="row">${cliente.getId()}</th>
      						<td>${cliente.getNome()}</td>
      						<td>${cliente.getCognome()}</td>
      						<td>${cliente.getEta()}</td>
      						<c:choose>
    							<c:when test="${cliente.getNazione()!=null}">
        							<td>${cliente.getNazione().getNome()}</td>
    							</c:when>    
    							<c:otherwise>
        							<td>Non Definito</td>
    							</c:otherwise>
							</c:choose>
							<c:choose>
    							<c:when test="${cliente.getConiuge()!=null}">
        							<td>${cliente.getConiuge().getNome()} ${cliente.getConiuge().getCognome()}</td>
    							</c:when>    
    							<c:otherwise>
        							<td>Non Definito</td>
    							</c:otherwise>
							</c:choose>
							<c:choose>
    							<c:when test="${!cliente.getContiCorrenti().isEmpty()}">
        							<td>
										<ul>
											<c:forEach items="${cliente.getContiCorrenti()}" var="contoCorrente">
												<li>${contoCorrente.getBanca().getName()}, <b>Saldo</b>: ${contoCorrente.getSaldo()}</li>
											</c:forEach>
										</ul>
									</td>
    							</c:when>    
    							<c:otherwise>
        							<td>-</td>
    							</c:otherwise>
							</c:choose>
      						<form method="POST" action="/eliminaCliente">
      							<td><button type="submit" id="idCliente" name="idCliente" class="btn btn-danger" value="${cliente.getId()}">Elimina</button></td>
      						</form>
      						<form method="POST" action="/modificaCliente">
      							<td><button type="submit" class="btn btn-warning" id="idCliente" name="idCliente" value="${cliente.getId()}">Modifica</button></td>
    						</form>
    					</tr>
    				</c:forEach>
  				</tbody>
			</table>
		</div>
		<a class="nav-link" href="/manageData">Indietro<span class="sr-only">(current)</span></a>
	</div>
	
	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>
</body>
</html>