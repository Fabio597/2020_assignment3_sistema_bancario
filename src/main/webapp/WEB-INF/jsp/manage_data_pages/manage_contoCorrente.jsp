<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Inserisci Conto Corrente</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron">
    		<h1>Aggiungi Conto Corrente</h1>      
    		<p>Arricchisci la base dati aggiungendo un Conto Corrente!!.</p>
  		</div>
  		<div class="jumbotron">
  			<h2>Aggiungi Conto Corrente</h2>
  			<form method="POST" action="/addContoCorrenteInDB">
  				<div class="form-group">
    				<label>Saldo Conto Corrente</label>
    				<input type="text" class="form-control" name="saldo" id="saldo" placeholder="Aggiungi Saldo Conto">
  				</div>
  				<div class="form-group">
    				<label for="exampleFormControlSelect1">Banca del Conto</label>
    				<select class="form-control" id="bancaContoCorrente" name="bancaContoCorrente">
      					<option value="0">Non Definito</option>
      					<c:forEach items="${banche}" var="banca">
      						<option value="${banca.getId()}">${banca.getName()}</option>
      					</c:forEach>
    				</select>
  				</div>
  				<button type="submit" class="btn btn-primary">Aggiungi Conto Corrente</button>
  			</form>
  		</div>
  		<div class="jumbotron">
			<h2>Conti Correnti gi� presenti</h2>
			<table class="table">
  				<thead>
    				<tr>
      					<th scope="col">#</th>
      					<th scope="col">Saldo</th>
      					<th scope="col">Banca</th>
      					<th scope="col">Clienti</th>
    				</tr>
  				</thead>
  				<tbody>
  					<c:forEach items="${contiCorrenti}" var="contoCorrente">
    					<tr>
      						<th scope="row">${contoCorrente.getId()}</th>
      						<td>${contoCorrente.getSaldo()}$</td>
      						<c:choose>
    							<c:when test="${contoCorrente.getBanca()!=null}">
        							<td>${contoCorrente.getBanca().getName()}</td>
    							</c:when>    
    							<c:otherwise>
        							<td>Non Definito</td>
    							</c:otherwise>
							</c:choose>
							<c:choose>
    							<c:when test="${!contoCorrente.getClienti().isEmpty()}">
    								<td>
										<ul>
											<c:forEach items="${contoCorrente.getClienti()}" var="cliente">
												<li>${cliente.getNome()} ${cliente.getCognome()}</li>
											</c:forEach>
										</ul>
									</td>
    							</c:when>
    							<c:otherwise>
        							<td>-</td>
    							</c:otherwise>
							</c:choose>
      						<form method="POST" action="/eliminaContoCorrente">
      							<td><button type="submit" id="idConto" name="idConto" class="btn btn-danger" value="${contoCorrente.getId()}">Elimina</button></td>
      					</form>
      						<form method="POST" action="/modificaContoCorrente">
      							<td><button type="submit" class="btn btn-warning" id="idConto" name="idConto" value="${contoCorrente.getId()}">Modifica</button></td>
    						</form>
    					</tr>	
    				</c:forEach>
  				</tbody>
			</table>
		</div>
		<a class="nav-link" href="/manageData">Indietro<span class="sr-only">(current)</span></a>
	</div>
	
	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>
</body>
</html>