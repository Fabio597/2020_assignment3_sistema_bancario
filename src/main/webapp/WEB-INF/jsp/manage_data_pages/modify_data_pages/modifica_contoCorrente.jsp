<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Modifica Conto Corrente</title>
	</head>
<body>

	<div class="container">
		<div class="jumbotron">
  			<h2>Modifica Conto Corrente</h2>
  			<form method="POST" action="/modificaContoCorrenteInDB">
  				<div class="form-group">
    				<label>Saldo Conto Corrente</label>
    				<input type="text" class="form-control" name="saldo" id="saldo" value="${contoCorrenteDaModificare.getSaldo()}">
  				</div>
  				<div class="form-group">
    				<label>Modifica Nazione</label>
    				<select class="form-control" id="bancaConto" name="bancaConto">
      					<c:choose>
      						<c:when test="${contoCorrenteDaModificare.getBanca() == null}">
      							<option selected value="0">Non Definito</option>
      							<c:forEach items="${banche}" var="banca">		
      								<option value="${banca.getId()}">${banca.getName()}</option>
      							</c:forEach>
      						</c:when>
      						<c:otherwise>
      							<option value="0">Non Definito</option>
      							<c:forEach items="${banche}" var="banca">
      								<c:choose>
      									<c:when test="${banca.getId() == contoCorrenteDaModificare.getBanca().getId()}">
      										<option selected value="${banca.getId()}">${banca.getName()}</option>
      									</c:when>
      									<c:otherwise>
      										<option value="${banca.getId()}">${banca.getName()}</option>
      									</c:otherwise>
      								</c:choose>
      							</c:forEach>
      						</c:otherwise>
      					</c:choose>
    				</select>
  				</div>
  					<button type="submit" class="btn btn-primary" id="idConto" name="idConto" value="${contoCorrenteDaModificare.getId()}">Modifica Conto Corrente</button>
			</form>
		</div>
		<div class="jumbotron">
  			<h2>Modifica Conto Corrente</h2>
			<form method="POST" action="/addClienteToContoInDB">
				<div class="form-group">
  					<label>Aggiungi Intestatario</label>
  					<select class="form-control" id="aggiungiIntestatario" name="aggiungiIntestatario">
  						<option value="0">Seleziona Cliente ...</option>
      					<c:forEach items="${clienti}" var="cliente">
      						<c:if test="${!contoCorrenteDaModificare.getClienti().contains(cliente)}">
      							<option value="${cliente.getId()}">${cliente.getNome()} ${cliente.getCognome()}</option>
      						</c:if>
      					</c:forEach>
  					</select>
  				</div>
  				<button type="submit" class="btn btn-primary" id="idConto" name="idConto" value="${contoCorrenteDaModificare.getId()}">Aggiungi Intestatario</button>
  			</form>
  		</div>
  		<div class="jumbotron">
  			<h2>Intestatari del Conto Corrente</h2>
  			<table class="table">
  				<thead>
    				<tr>
      					<th scope="col">#</th>
      					<th scope="col">Intestatari</th>
    				</tr>
  				</thead>
  				<tbody>
  					<c:forEach items="${contoCorrenteDaModificare.getClienti()}" var="intestatario">
    					<tr>
    						<td>${intestatario.getId()}</td>
    						<td>${intestatario.getNome()} ${intestatario.getCognome()}</td>
      						<form method="POST" action="/rimuoviIntestatario">
      							<td><button type="submit" id="idConto" name="idConto" class="btn btn-danger" value="${contoCorrenteDaModificare.getId()}">Rimuovi</button></td>
      							<input type="hidden" id="idIntestatario" name="idIntestatario" value="${intestatario.getId()}">
      						</form>
    					</tr>	
    				</c:forEach>
  				</tbody>
			</table>
  		</div>
  		<a class="nav-link" href="/manageContoCorrente">Indietro<span class="sr-only">(current)</span></a>
	</div>

	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>

</body>
</html>