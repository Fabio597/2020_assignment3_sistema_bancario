<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Modifica Cliente</title>
	</head>
<body>

	<div class="container">
		<div class="jumbotron">
  			<h2>Modifica Cliente</h2>
  			<form method="POST" action="/modificaClienteInDB">
  				<div class="form-group">
    				<label>Nome Cliente da Modificare</label>
    				<input type="text" class="form-control" name="nomeCliente" id="nomeCliente" value="${clienteDaModificare.getNome()}">
  				</div>
  				<div class="form-group">
    				<label>Cognome Cliente da Modificare</label>
    				<input type="text" class="form-control" name="cognomeCliente" id="cognomeCliente" value="${clienteDaModificare.getCognome()}">
  				</div>
  				<div class="form-group">
    				<label>Eta Cliente da Modificare</label>
    				<input type="text" class="form-control" name="etaCliente" id="etaCliente" value="${clienteDaModificare.getEta()}">
  				</div>
  				<div class="form-group">
    				<label>Nazione Cliente</label>
    				<select class="form-control" id="nazioneCliente" name="nazioneCliente">
      					<c:choose>
      						<c:when test="${clienteDaModificare.getNazione() == null}">
      							<option selected value="0">Non Definito</option>
      							<c:forEach items="${nazioni}" var="nazione">		
      								<option value="${nazione.getNazioneId()}">${nazione.getNome()}</option>
      							</c:forEach>
      						</c:when>
      						<c:otherwise>
      							<option value="0">Non Definito</option>
      							<c:forEach items="${nazioni}" var="nazione">
      								<c:choose>
      									<c:when test="${nazione.getNazioneId() == clienteDaModificare.getNazione().getNazioneId()}">
      										<option selected value="${nazione.getNazioneId()}">${nazione.getNome()}</option>
      									</c:when>
      									<c:otherwise>
      										<option value="${nazione.getNazioneId()}">${nazione.getNome()}</option>
      									</c:otherwise>
      								</c:choose>
      							</c:forEach>
      						</c:otherwise>
      					</c:choose>
    				</select>
  				</div>
  				<div class="form-group">
    				<label>Coniuge Cliente</label>
    				<select class="form-control" id="coniugeCliente" name="coniugeCliente">
      					<c:choose>
      						<c:when test="${clienteDaModificare.getConiuge() == null}">
      							<option selected value="0">Non Definito</option>
      							<c:forEach items="${clienti}" var="cliente">	
      								<c:if test="${cliente.getId() != clienteDaModificare.getId()}">	
      									<option value="${cliente.getId()}">${cliente.getNome()} ${cliente.getCognome()}</option>
      								</c:if>
      							</c:forEach>
      						</c:when>
      						<c:otherwise>
      							<option value="0">Non Definito</option>
      							<c:forEach items="${clienti}" var="cliente">
      								<c:choose>
      									<c:when test="${cliente.getId() == clienteDaModificare.getConiuge().getId()}">
      										<option selected value="${cliente.getId()}">${cliente.getNome()} ${cliente.getCognome()}</option>
      									</c:when>
      									<c:otherwise>
      										<c:if test="${cliente.getId() != clienteDaModificare.getId()}">	
      											<option value="${cliente.getId()}">${cliente.getNome()} ${cliente.getCognome()}</option>
      										</c:if>
      									</c:otherwise>
      								</c:choose>
      							</c:forEach>
      						</c:otherwise>
      					</c:choose>
    				</select>
  				</div>
  					<button type="submit" class="btn btn-primary" id="idCliente" name="idCliente" value="${clienteDaModificare.getId()}">Modifica Cliente</button>
			</form>
		</div>
		<a class="nav-link" href="/manageCliente">Indietro<span class="sr-only">(current)</span></a>
	</div>

	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>

</body>
</html>