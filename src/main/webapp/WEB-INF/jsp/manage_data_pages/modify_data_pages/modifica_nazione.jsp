<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Modifica Nazione</title>
	</head>
<body>

	<div class="container">
		<div class="jumbotron">
  			<h2>Modifica Nazione</h2>
  			<form method="POST" action="/modificaNazioneInDB">
  				<div class="form-group">
    				<label>Nome Nazione da Modificare</label>
    				<input type="text" class="form-control" name="nomeNazione" id="nomeNazione" value="${nazioneDaModificare.getNome()}">
  						</div>
  					<button type="submit" class="btn btn-primary" id="idNazione" name="idNazione" value="${nazioneDaModificare.getNazioneId()}">Modifica Nazione</button>
			</form>
		</div>
		<a class="nav-link" href="/manageNazione">Indietro<span class="sr-only">(current)</span></a>
	</div>

	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>

</body>
</html>