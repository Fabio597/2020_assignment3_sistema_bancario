<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Modifica Lavoratore</title>
	</head>
<body>

	<div class="container">
		<div class="jumbotron">
		<c:choose>
      		<c:when test="${tipoLavoratore==0}">
      			<h2>Modifica Direttore</h2>
      		</c:when>
      		<c:otherwise>
      			<h2>Modifica Impiegato</h2>
      		</c:otherwise>
      	</c:choose>
			<form method="POST" action="/modificaLavoratoreInDB">
				<div class="form-group">
    				<label>Modifica Nome Lavoratore</label>
   					<input type="text" class="form-control" name="nomeLavoratore" id="nomeLavoratore" value="${lavoratoreDaModificare.getNome()}">
  				</div>
  				<div class="form-group">
    				<label>Modifica Cognome Lavoratore</label>
   					<input type="text" class="form-control" name="cognomeLavoratore" id="cognomeLavoratore" value="${lavoratoreDaModificare.getCognome()}">
  				</div>
  				<div class="form-group">
    				<label>Modifica Cognome Lavoratore</label>
   					<input type="text" class="form-control" name="etaLavoratore" id="etaLavoratore" value="${lavoratoreDaModificare.getEta()}">
  				</div>
  				<div class="form-group">
    				<label>Seleziona Banca</label>
    				<select class="form-control" id="bancaLavoratore" name="bancaLavoratore">
      					<c:choose>
      						<c:when test="${lavoratoreDaModificare.getBanca() == null}">
      							<option selected value="0">Non Definito</option>
      							<c:forEach items="${banche}" var="banca">		
      								<option value="${banca.getId()}">${banca.getName()}</option>
      							</c:forEach>
      						</c:when>
      						<c:otherwise>
      							<option value="0">Non Definito</option>
      							<c:forEach items="${banche}" var="banca">
      								<c:choose>
      									<c:when test="${banca.getId() == lavoratoreDaModificare.getBanca().getId()}">
      										<option selected value="${banca.getId()}">${banca.getName()}</option>
      									</c:when>
      									<c:otherwise>
      										<option value="${banca.getId()}">${banca.getName()}</option>
      									</c:otherwise>
      								</c:choose>
      							</c:forEach>
      						</c:otherwise>
      					</c:choose>
    				</select>
  				</div>
  				<c:choose>
      				<c:when test="${tipoLavoratore==0}">
      					<div class="form-group">
    						<label>Modifica Ufficio</label>
   							<input type="text" class="form-control" name="ufficioSportello" id="ufficioSportello" value="${lavoratoreDaModificare.getNumeroUfficio()}">
  						</div>
      				</c:when>
      				<c:otherwise>
      					<div class="form-group">
    						<label>Modifica Sportello</label>
   							<input type="text" class="form-control" name="ufficioSportello" id="ufficioSportello" value="${lavoratoreDaModificare.getNumeroSportello()}">
  						</div>
      				</c:otherwise>
      			</c:choose>
  				<c:choose>
      				<c:when test="${tipoLavoratore==0}">
      					<button type="submit" class="btn btn-primary" id="tipoLavoratore" name="tipoLavoratore" value="${tipoLavoratore}">Modifica Direttore</button>
      					 <input type="hidden" id="idLavoratore" name="idLavoratore" value="${lavoratoreDaModificare.getId()}">
      				</c:when>
      				<c:otherwise>
      					<button type="submit" class="btn btn-primary" id="tipoLavoratore" name="tipoLavoratore" value="${tipoLavoratore}">Modifica Impiegato</button>
      					 <input type="hidden" id="idLavoratore" name="idLavoratore" value="${lavoratoreDaModificare.getId()}">
      				</c:otherwise>
      			</c:choose>
  			</form>
  		</div>
  		<a class="nav-link" href="/manageLavoratore">Indietro<span class="sr-only">(current)</span></a>
	</div>

	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>

</body>
</html>