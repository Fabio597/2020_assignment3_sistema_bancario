<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Inserisci Lavoratore</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron">
    		<h1>Aggiungi Lavoratore</h1>      
    		<p>Arricchisci la base dati aggiungendo un Lavoratore o una Lavoratrice!!.</p>
  		</div>
  		<div class="jumbotron">
  			<h2>Aggiungi Lavoratore</h2>
  			<form method="POST" action="/addLavoratoreInDB">
  				<div class="form-group">
    				<label>Nome Lavoratore</label>
    				<input type="text" class="form-control" name="nomeLavoratore" id="nomeLavoratore" placeholder="Aggiungi Nome Lavoratore">
  				</div>
  				<div class="form-group">
    				<label>Cognome Cliente</label>
    				<input type="text" class="form-control" name="cognomeLavoratore" id="cognomeLavoratore" placeholder="Aggiungi Cognome Lavoratore">
  				</div>
  				<div class="form-group">
    				<label>Et� Cliente</label>
    				<input type="text" class="form-control" name="etaLavoratore" id="etaLavoratore" placeholder="Aggiungi Et� Lavoratore">
  				</div>
  				<div class="form-group">
    				<label for="exampleFormControlSelect1">Seleziona Banca del Lavoratore</label>
    				<select class="form-control" id="bancaLavoratore" name="bancaLavoratore">
      					<option value="0">Non Definito</option>
      					<c:forEach items="${banche}" var="banca">
      						<option value="${banca.getId()}">${banca.getName()}</option>
      					</c:forEach>
    				</select>
  				</div>
  				<div class="form-check">
  					<input class="form-check-input" type="radio" name="tipoLavoratore" id="direttore" value="0" checked>
  					<label class="form-check-label">
    					Direttore
  					</label>
				</div>
				<div class="form-check">
  					<input class="form-check-input" type="radio" name="tipoLavoratore" id="impiegato" value="1">
  					<label class="form-check-label">
    					Impiegato
  					</label>
				</div>
				<div class="form-group">
    				<label>Numero Ufficio/Sportello</label>
    				<input type="text" class="form-control" name="ufficioSportello" id="ufficioSportello" placeholder="Aggiungi Ufficio/Sportello">
  				</div>
				<br>
  				<button type="submit" class="btn btn-primary">Aggiungi Lavoratore</button>
  			</form>
  		</div>
  		<div class="jumbotron">
			<h2>Direttori gi� presenti</h2>
			<table class="table">
  				<thead>
    				<tr>
      					<th scope="col">#</th>
      					<th scope="col">Nome</th>
      					<th scope="col">Cognome</th>
      					<th scope="col">Et�</th>
      					<th scope="col">Numero Ufficio</th>
      					<th scope="col">Banca</th>
    				</tr>
  				</thead>
  				<tbody>
  					<c:forEach items="${direttori}" var="direttore">
    					<tr>
      						<th scope="row">${direttore.getId()}</th>
      						<td>${direttore.getNome()}</td>
      						<td>${direttore.getCognome()}</td>
      						<td>${direttore.getEta()}</td>
      						<td>${direttore.getNumeroUfficio()}</td>
							<c:choose>
    							<c:when test="${direttore.getBanca()!=null}">
        							<td>${direttore.getBanca().getName()}</td>
    							</c:when>    
    							<c:otherwise>
        							<td>Non Definito</td>
    							</c:otherwise>
							</c:choose>
      						<form method="POST" action="/eliminaDirettore">
      							<td><button type="submit" id="idDirettore" name="idDirettore" class="btn btn-danger" value="${direttore.getId()}">Elimina</button></td>
      						</form>
      						<form method="POST" action="/modificaDirettore">
      							<td><button type="submit" class="btn btn-warning" id="idDirettore" name="idDirettore" value="${direttore.getId()}">Modifica</button></td>
    						</form>
    					</tr>
    				</c:forEach>
  				</tbody>
			</table>
			</div>
			<div class="jumbotron">
			<h2>Impiegati gi� presenti</h2>
			<table class="table">
  				<thead>
    				<tr>
      					<th scope="col">#</th>
      					<th scope="col">Nome</th>
      					<th scope="col">Cognome</th>
      					<th scope="col">Et�</th>
      					<th scope="col">Numero Sportello</th>
      					<th scope="col">Banca</th>
    				</tr>
  				</thead>
  				<tbody>
  					<c:forEach items="${impiegati}" var="impiegato">
    					<tr>
      						<th scope="row">${impiegato.getId()}</th>
      						<td>${impiegato.getNome()}</td>
      						<td>${impiegato.getCognome()}</td>
      						<td>${impiegato.getEta()}</td>
      						<td>${impiegato.getNumeroSportello()}</td>
							<c:choose>
    							<c:when test="${impiegato.getBanca()!=null}">
        							<td>${impiegato.getBanca().getName()}</td>
    							</c:when>    
    							<c:otherwise>
        							<td>Non Definito</td>
    							</c:otherwise>
							</c:choose>
      						<form method="POST" action="/eliminaImpiegato">
      							<td><button type="submit" id="idImpiegato" name="idImpiegato" class="btn btn-danger" value="${impiegato.getId()}">Elimina</button></td>
      						</form>
      						<form method="POST" action="/modificaImpiegato">
      							<td><button type="submit" class="btn btn-warning" id="idImpiegato" name="idImpiegato" value="${impiegato.getId()}">Modifica</button></td>
    						</form>
    					</tr>
    				</c:forEach>
  				</tbody>
			</table>
		</div>
		<a class="nav-link" href="/manageData">Indietro<span class="sr-only">(current)</span></a>
	</div>
	
	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>
</body>
</html>