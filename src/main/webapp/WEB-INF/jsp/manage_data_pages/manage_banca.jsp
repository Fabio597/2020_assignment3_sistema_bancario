<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Inserisci Banca</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron">
    		<h1>Aggiungi Banca</h1>      
    		<p>Arricchisci la base dati aggiungendo una Banca!!.</p>
  		</div>
  		<div class="jumbotron">
  			<h2>Aggiungi Banca</h2>
  			<form method="POST" action="/addBancaInDB">
  				<div class="form-group">
    				<label>Nome Banca</label>
    				<input type="text" class="form-control" name="nomeBanca" id="nomeBanca" placeholder="Aggiungi Nome Banca">
  				</div>
  				<div class="form-group">
    				<label for="exampleFormControlSelect1">Example select</label>
    				<select class="form-control" id="nazioneBanca" name="nazioneBanca">
      					<option value="0">Non Definito</option>
      					<c:forEach items="${nazioni}" var="nazione">
      						<option value="${nazione.getNazioneId()}">${nazione.getNome()}</option>
      					</c:forEach>
    				</select>
  				</div>
  				<button type="submit" class="btn btn-primary">Aggiungi Banca</button>
			</form>
		</div>
		<div class="jumbotron">
			<h2>Banche gi� presenti</h2>
			<table class="table">
  				<thead>
    				<tr>
      					<th scope="col">#</th>
      					<th scope="col">Banca</th>
      					<th scope="col">Nazione</th>
    				</tr>
  				</thead>
  				<tbody>
  					<c:forEach items="${banche}" var="banca">
    					<tr>
      						<th scope="row">${banca.getId()}</th>
      						<td>${banca.getName()}</td>
      						<c:choose>
    							<c:when test="${banca.getNazione()!=null}">
        							<td>${banca.getNazione().getNome()}</td>
    							</c:when>    
    							<c:otherwise>
        							<td>Non Definito</td>
    							</c:otherwise>
							</c:choose>
      						<form method="POST" action="/eliminaBanca">
      							<td><button type="submit" id="idBanca" name="idBanca" class="btn btn-danger" value="${banca.getId()}">Elimina</button></td>
      						</form>
      						<form method="POST" action="/modificaBanca">
      							<td><button type="submit" class="btn btn-warning" id="idBanca" name="idBanca" value="${banca.getId()}">Modifica</button></td>
    						</form>
    					</tr>
    				</c:forEach>
  				</tbody>
			</table>
		</div>
		<a class="nav-link" href="/manageData">Indietro<span class="sr-only">(current)</span></a>
	</div>
	
	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>
</body>
</html>