<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Inserisci Nazione</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron">
    		<h1>Gestisci Nazioni</h1>      
    		<p>Arricchisci la base dati aggiungendo una Nazione!!.</p>
  		</div>
  		
  		<div class="jumbotron">
  			<h2>Aggiungi Nazione</h2>
  			<form method="POST" action="/addNazioneInDB">
  				<div class="form-group">
    				<label>Nome Nazione</label>
    				<input type="text" class="form-control" name="nomeNazione" id="nomeNazione" placeholder="Aggiungi Nome Nazione">
  				</div>
  				<button type="submit" class="btn btn-primary">Aggiungi Nazione</button>
			</form>
		</div>
		<br>
		<div class="jumbotron">
			<h2>Nazioni gi� presenti</h2>
			<table class="table">
  				<thead>
    				<tr>
      					<th scope="col">#</th>
      					<th scope="col">Nazione</th>
    				</tr>
  				</thead>
  				<tbody>
  					<c:forEach items="${nazioni}" var="nazione">
    					<tr>
      						<th scope="row">${nazione.getNazioneId()}</th>
      						<td>${nazione.getNome()}</td>
      						<form method="POST" action="\eliminaNazione">
      							<td><button type="submit" id="idNazione" name="idNazione" class="btn btn-danger" value="${nazione.getNazioneId()}">Elimina</button></td>
      						</form>
      						<form method="POST" action="\modificaNazione">
      							<td><button type="submit" class="btn btn-warning" id="idNazione" name="idNazione" value="${nazione.getNazioneId()}">Modifica</button></td>
    						</form>
    					</tr>
    				</c:forEach>
  				</tbody>
			</table>
		</div>
		<a class="nav-link" href="/manageData">Indietro<span class="sr-only">(current)</span></a>
	</div>

	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>
</body>
</html>