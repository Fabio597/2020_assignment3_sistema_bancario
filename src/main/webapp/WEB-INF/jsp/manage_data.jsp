<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Add Data Page</title>
</head>
<body>

	<div class="container">
		<div class="jumbotron">
    		<h1>Aggiungi Dati</h1>      
    		<p>Arricchisci la base dati aggiungendo informazioni!!!.</p>
  		</div>
		
		<form method="POST" action="/manageNazione">
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Gestisci Nazione</button>
			</div>
		</form>
		
		<form method="POST" action="/manageBanca">
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Gestisci Banca</button>
			</div>
		</form>
		
		<form method="POST" action="/manageCliente">
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Gestisci Cliente</button>
			</div>
		</form>
		
		<form method="POST" action="/manageLavoratore">
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Gestisci Lavoratore</button>
			</div>
		</form>
		
		<form method="POST" action="/manageContoCorrente">
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Gestisci Conti Correnti</button>
			</div>
		</form>
		<a class="nav-link" href="/">Indietro<span class="sr-only">(current)</span></a>
	</div>

	<script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</script>
</body>
</html>