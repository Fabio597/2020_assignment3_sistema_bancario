package com.assignment.sistemabancario.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.sistemabancario.model.db.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {}
