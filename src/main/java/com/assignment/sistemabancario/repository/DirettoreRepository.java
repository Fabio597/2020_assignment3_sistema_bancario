package com.assignment.sistemabancario.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.sistemabancario.model.db.Direttore;

public interface DirettoreRepository extends JpaRepository<Direttore, Long>{}
