package com.assignment.sistemabancario.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.assignment.sistemabancario.model.db.ContoCorrente;

public interface ContoCorrenteRepository extends JpaRepository<ContoCorrente, Long> {
	
	@Query(value = "select * from conti_correnti order by saldo desc", nativeQuery = true)
	public List<ContoCorrente> contiOrdinatiPerSaldo();
}
