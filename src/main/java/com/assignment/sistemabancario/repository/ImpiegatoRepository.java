package com.assignment.sistemabancario.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.sistemabancario.model.db.Impiegato;

public interface ImpiegatoRepository extends JpaRepository<Impiegato, Long> {}
