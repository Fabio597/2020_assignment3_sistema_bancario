package com.assignment.sistemabancario.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.sistemabancario.model.db.Nazione;

public interface NazioneRepository extends JpaRepository<Nazione, Long>{
}
