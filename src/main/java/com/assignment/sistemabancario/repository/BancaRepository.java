package com.assignment.sistemabancario.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.assignment.sistemabancario.model.db.Banca;

public interface BancaRepository extends JpaRepository<Banca, Long>{
	
	@Query(value = "select * from banca order by nome asc", nativeQuery = true)
	public List<Banca> bancheOrdinatePerNome();
}
