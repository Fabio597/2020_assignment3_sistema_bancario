package com.assignment.sistemabancario.model.db;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("0")
public class Direttore extends Lavoratore {

	@Column(name = "Ufficio")
	private int numeroUfficio;
	
	public Direttore() {}
	
	public Direttore(String nome, String cognome, int eta, int numeroUfficio) {
		super(nome, cognome, eta);
		this.numeroUfficio = numeroUfficio;
	}
	
	public Direttore(String nome, String cognome, int eta, Banca banca, int numeroUfficio) {
		super(nome, cognome, eta, banca);
		this.numeroUfficio = numeroUfficio;
	}

	public int getNumeroUfficio() {
		return numeroUfficio;
	}

	public void setNumeroUfficio(int numeroUfficio) {
		this.numeroUfficio = numeroUfficio;
	}
}
