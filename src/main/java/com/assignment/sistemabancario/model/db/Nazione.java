package com.assignment.sistemabancario.model.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;

@Entity
@Table(name = "nazione")
public class Nazione {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long nazioneId;
	
	@Column(name = "nome")
	private String nome;
	
	@OneToMany(mappedBy = "nazione")
	private List<Banca> banche = new ArrayList<Banca>();
	
	@OneToMany(mappedBy = "nazione")
	private List<Cliente> clienti = new ArrayList<Cliente>();
	
	public Nazione() {}
	
	public Nazione(String nome) {
		this.nome = nome;
	}
	
	@PreRemove
	public void setNullOnDelete() {
		this.banche.forEach(banca -> banca.setNazione(null));
		this.clienti.forEach(cliente -> cliente.setNazione(null));
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getNazioneId() {
		return nazioneId;
	}

	public List<Banca> getBanche() {
		return banche;
	}

	public void setBanche(List<Banca> banche) {
		this.banche = banche;
	}
}
