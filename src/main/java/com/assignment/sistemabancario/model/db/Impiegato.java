package com.assignment.sistemabancario.model.db;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
public class Impiegato extends Lavoratore {

	@Column(name = "Sportello")
	private int numeroSportello;
	
	public Impiegato() {}
	
	public Impiegato(String nome, String cognome, int eta, int numeroSportello) {
		super(nome, cognome, eta);
		this.numeroSportello = numeroSportello;
	}
	
	public Impiegato(String nome, String cognome, int eta, Banca banca, int numeroSportello) {
		super(nome, cognome, eta, banca);
		this.numeroSportello = numeroSportello;
	}

	public int getNumeroSportello() {
		return numeroSportello;
	}

	public void setNumeroSportello(int numeroSportello) {
		this.numeroSportello = numeroSportello;
	}
}
