package com.assignment.sistemabancario.model.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "banca")
public class Banca {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long bancaId;
	
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@ManyToOne
	@JoinColumn(name = "nazioneId")
	private Nazione nazione;
	
	@OneToMany(mappedBy = "banca")
	private List<ContoCorrente> contiCorrenti = new ArrayList<ContoCorrente>();
	
	@OneToMany(mappedBy = "banca")
	private List<Lavoratore> lavoratori = new ArrayList<Lavoratore>();
	
	@Transient
	private int numeroClienti = 0;
	
	public Banca() {}
	
	public Banca(String nome) {
		super();
		this.nome = nome;
	}
	
	public Banca(String nome, Nazione nazione) {
		super();
		this.nome = nome;
		this.nazione = nazione;
	}
	
	public long getId() {
		return bancaId;
	}

	public String getName() {
		return nome;
	}

	public void setName(String nome) {
		this.nome = nome;
	}

	public Nazione getNazione() {
		return nazione;
	}

	public void setNazione(Nazione nazione) {
		this.nazione = nazione;
	}
	
	public List<ContoCorrente> getContiCorrenti() {
		return contiCorrenti;
	}

	public int getNumeroClienti() {
		return numeroClienti;
	}

	public void setNumeroClienti(int numeroClienti) {
		this.numeroClienti = numeroClienti;
	}

	@PreRemove
	public void setNullOnDelete() {
		this.contiCorrenti.forEach(contiCorrenti -> contiCorrenti.setBanca(null));
		this.lavoratori.forEach(lavoratore -> lavoratore.setBanca(null));
		if (this.nazione!= null) {
			this.nazione.getBanche().remove(this);
		}
	}
}