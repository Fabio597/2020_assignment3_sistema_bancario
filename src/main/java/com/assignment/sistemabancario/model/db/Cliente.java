package com.assignment.sistemabancario.model.db;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;

@Entity
@Table(name = "clienti")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "Nome", nullable = false)
	private String nome;
	
	@Column(name = "Cognome", nullable = false)
	private String cognome;
	
	@Column(name = "Eta", nullable = false)
	private int eta;
	
	@ManyToOne
	@JoinColumn(name = "nazioneId")
	private Nazione nazione;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="coniuge_id")
	private Cliente coniuge;
	
	@ManyToMany(mappedBy = "clienti")
    Set<ContoCorrente> contiCorrenti = new HashSet<ContoCorrente>();
	
	public Cliente() {}

	public Cliente(String nome, String cognome, int eta) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.eta = eta;
	}

	public Cliente(String nome, String cognome, int eta, Nazione nazione) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.eta = eta;
		this.nazione = nazione;
	}
	
	public Cliente(String nome, String cognome, int eta, Nazione nazione, Cliente coniuge) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.eta = eta;
		this.nazione = nazione;
		this.coniuge = coniuge;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}

	public Set<ContoCorrente> getContiCorrenti() {
		return contiCorrenti;
	}

	public void setContiCorrenti(Set<ContoCorrente> contiCorrenti) {
		this.contiCorrenti = contiCorrenti;
	}

	public long getId() {
		return id;
	}

	public Cliente getConiuge() {
		return coniuge;
	}

	public void setConiuge(Cliente coniuge) {
		this.coniuge = coniuge;
	}

	public void addConto(ContoCorrente contoCorrente) {
		this.contiCorrenti.add(contoCorrente);
	}

	public Nazione getNazione() {
		return nazione;
	}

	public void setNazione(Nazione nazione) {
		this.nazione = nazione;
	}
	
	
	@PreRemove
	public void setNullBeforeRemove() {
		if (this.getConiuge() != null)
			this.getConiuge().setConiuge(null);
		this.setConiuge(null);
		if (!this.getContiCorrenti().isEmpty()) {
			for (ContoCorrente contoCorrente : this.getContiCorrenti()) {
				contoCorrente.getClienti().remove(this);
			}
		}
	}
}
