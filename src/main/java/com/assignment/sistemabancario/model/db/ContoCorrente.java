package com.assignment.sistemabancario.model.db;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "conti_correnti")
public class ContoCorrente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "Saldo", nullable = false)
	private int saldo;
	
	@ManyToMany
    @JoinTable(
      name = "clienti_conti", 
      joinColumns = @JoinColumn(name = "contoCorrente_id"), 
      inverseJoinColumns = @JoinColumn(name = "cliente_id"))
	Set<Cliente> clienti = new HashSet<Cliente>();
	
	@ManyToOne
	@JoinColumn(name = "bancaId")
	private Banca banca;
	
	public ContoCorrente() {}

	public ContoCorrente(int saldo) {
		super();
		this.saldo = saldo;
	}

	public ContoCorrente(int saldo, Banca banca) {
		super();
		this.saldo = saldo;
		this.banca = banca;
	}
	
	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	
	public Banca getBanca() {
		return banca;
	}
	
	public void setBanca(Banca banca) {
		this.banca = banca;
	}

	public Set<Cliente> getClienti() {
		return clienti;
	}

	public void setClienti(Set<Cliente> clienti) {
		this.clienti = clienti;
	}

	public long getId() {
		return id;
	}
	
	public void addClienteToConto(Cliente cliente) {
		this.clienti.add(cliente);
	}
	
	public void removeCliente(Cliente cliente) {
		this.clienti.remove(cliente);
	}
}
