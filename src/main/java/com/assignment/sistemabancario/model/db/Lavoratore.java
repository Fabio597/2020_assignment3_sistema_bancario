package com.assignment.sistemabancario.model.db;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lavoratori")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo_lavoratore", 
discriminatorType = DiscriminatorType.INTEGER)
public class Lavoratore {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "Nome", nullable = false)
	private String nome;
	
	@Column(name = "Cognome", nullable = false)
	private String cognome;
	
	@Column(name = "eta", nullable = false)
	private int eta;
	
	@ManyToOne
	@JoinColumn(name = "bancaId")
	private Banca banca;

	public Lavoratore() {}
	
	public Lavoratore(String nome, String cognome, int eta) {
		this.nome = nome;
		this.cognome = cognome;
		this.eta = eta;
	}
	
	public Lavoratore(String nome, String cognome, int eta, Banca banca) {
		this.nome = nome;
		this.cognome = cognome;
		this.eta = eta;
		this.banca = banca;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}

	public Banca getBanca() {
		return banca;
	}

	public void setBanca(Banca banca) {
		this.banca = banca;
	}

	public long getId() {
		return id;
	}
}
