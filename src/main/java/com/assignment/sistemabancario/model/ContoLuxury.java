package com.assignment.sistemabancario.model;

import java.util.Set;

import com.assignment.sistemabancario.model.db.Banca;
import com.assignment.sistemabancario.model.db.Cliente;
import com.assignment.sistemabancario.model.db.ContoCorrente;

public class ContoLuxury {

	private Banca bancaLuxury;
	
	private ContoCorrente contoLuxury;
	
	private Set<Cliente> clientiLuxury;

	public ContoLuxury() {
		super();
	}
	
	public ContoLuxury(Banca bancaLuxury, ContoCorrente contoLuxury) {
		super();
		this.bancaLuxury = bancaLuxury;
		this.contoLuxury = contoLuxury;
	}
	
	public ContoLuxury(Banca bancaLuxury, ContoCorrente contoLuxury, Set<Cliente> clientiLuxury) {
		super();
		this.bancaLuxury = bancaLuxury;
		this.contoLuxury = contoLuxury;
		this.clientiLuxury = clientiLuxury;
	}

	public Banca getBancaLuxury() {
		return bancaLuxury;
	}

	public void setBancaLuxury(Banca bancaLuxury) {
		this.bancaLuxury = bancaLuxury;
	}

	public ContoCorrente getContoLuxury() {
		return contoLuxury;
	}

	public void setContoLuxury(ContoCorrente contoLuxury) {
		this.contoLuxury = contoLuxury;
	}

	public Set<Cliente> getClientiLuxury() {
		return clientiLuxury;
	}

	public void setClientiLuxury(Set<Cliente> clientiLuxury) {
		this.clientiLuxury = clientiLuxury;
	}
	
	public void addClienteToCotoLuxury(Cliente clienteLuxury) {
		this.clientiLuxury.add(clienteLuxury);
	}
}

