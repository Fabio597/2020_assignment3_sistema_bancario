package com.assignment.sistemabancario.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.sistemabancario.model.db.Banca;
import com.assignment.sistemabancario.model.db.ContoCorrente;
import com.assignment.sistemabancario.repository.BancaRepository;
import com.assignment.sistemabancario.repository.ClienteRepository;
import com.assignment.sistemabancario.repository.ContoCorrenteRepository;
import com.assignment.sistemabancario.repository.DirettoreRepository;
import com.assignment.sistemabancario.repository.ImpiegatoRepository;
import com.assignment.sistemabancario.repository.NazioneRepository;

@Service
public class DBOperations {

	@Autowired
	NazioneRepository nazioneRepository;
	
	@Autowired
	BancaRepository bancaRepository;
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	ContoCorrenteRepository contoCorrenteRepository;
	
	@Autowired
	DirettoreRepository direttoreRepository;
	
	@Autowired
	ImpiegatoRepository impiegatoRepository;
	
	public NazioneRepository getNazioneRepository() {
		return nazioneRepository;
	}

	public BancaRepository getBancaRepository() {
		return bancaRepository;
	}

	public ClienteRepository getClienteRepository() {
		return clienteRepository;
	}

	public ContoCorrenteRepository getContoCorrenteRepository() {
		return contoCorrenteRepository;
	}

	public ImpiegatoRepository getImpiegatoRepository() {
		return impiegatoRepository;
	}
	
	public DirettoreRepository getDirettoreRepository() {
		return direttoreRepository;
	}

	public List<ContoLuxury> getContiLuxury(int value) {
		ArrayList<ContoLuxury> contiLuxury = new ArrayList<ContoLuxury>();
		for (ContoCorrente conto : contoCorrenteRepository.findAll()) {
			if (conto.getSaldo() >= value) {
				contiLuxury.add(new ContoLuxury(conto.getBanca(), conto, conto.getClienti()));
			}
		}
		return contiLuxury;
	}
	
	public List<Banca> getBancheConNClienti(int n) {
		int numClienti = 0;
		ArrayList<Banca> banche = new ArrayList<Banca>();
		for (Banca banca : bancaRepository.findAll()) {
			numClienti = 0;
			for (ContoCorrente conto : banca.getContiCorrenti()) {
				numClienti += conto.getClienti().size();
			}
			if (numClienti >= n) {
				banca.setNumeroClienti(numClienti);
				banche.add(banca);
			}
		}
		return banche;
	}
}
