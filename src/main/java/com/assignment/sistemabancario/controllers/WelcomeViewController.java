package com.assignment.sistemabancario.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Banca;
import com.assignment.sistemabancario.model.db.Cliente;
import com.assignment.sistemabancario.model.db.ContoCorrente;
import com.assignment.sistemabancario.model.db.Direttore;
import com.assignment.sistemabancario.model.db.Impiegato;
import com.assignment.sistemabancario.model.db.Nazione;

@Controller
public class WelcomeViewController {
	
	private boolean addDefaultDataVisible = true;
	
	@Autowired
	DBOperations dbOperations;
	
	@RequestMapping(value = "/")
	public ModelAndView welcome() {
		ModelAndView mvWelcome = new ModelAndView("welcome");
		mvWelcome.addObject("banche", dbOperations.getBancaRepository().bancheOrdinatePerNome());
		mvWelcome.addObject("addDefaultDataVisible", addDefaultDataVisible);
		mvWelcome.addObject("contiLuxury", dbOperations.getContiLuxury(5000));
		mvWelcome.addObject("contiOrdinatiPerSaldo", dbOperations.getContoCorrenteRepository().contiOrdinatiPerSaldo());
		mvWelcome.addObject("bancheConNClienti", dbOperations.getBancheConNClienti(4));
		return mvWelcome;
	}
	
	@RequestMapping(value = "/manageData")
	public ModelAndView addDataClicked() {
		ModelAndView mvDataClicked = new ModelAndView("manage_data");
		return mvDataClicked;
	}
	
	@RequestMapping(value = "/addDefaultData")
	public ModelAndView addDefaultData() {
		this.addDefaultDataVisible = false;
		this.addData();
		ModelAndView mvAddDefaultData = new ModelAndView(new RedirectView("/"));
		return mvAddDefaultData;
	}
	
	public void addData() {
		// Nazioni
		Nazione italia = new Nazione("Italia");
		Nazione svizzera = new Nazione("Svizzera");
		Nazione spagna = new Nazione("Spagna");
		Nazione francia = new Nazione("Francia");
		Nazione inghilterra = new Nazione("Inghilterra");
		dbOperations.getNazioneRepository().save(italia);
		dbOperations.getNazioneRepository().save(svizzera);
		dbOperations.getNazioneRepository().save(francia);
		dbOperations.getNazioneRepository().save(spagna);
		dbOperations.getNazioneRepository().save(inghilterra);
		// Banche
		Banca unicredit = new Banca("Unicredit", italia);
		Banca ubi = new Banca("UBI", italia);
		Banca bpm = new Banca("Banca Popolare di Milano", italia);
		Banca swissBank = new Banca("Swiss Bank", svizzera);
		Banca ubs = new Banca("UBS", svizzera); 
		Banca bbva = new Banca("BBVA", spagna);
		Banca creditAgricole = new Banca("Credìt Agricolè", francia);
		Banca barclays = new Banca("Barclays", inghilterra);
		Banca hsbc = new Banca("HSBC", inghilterra);
		dbOperations.getBancaRepository().save(unicredit);
		dbOperations.getBancaRepository().save(ubi);
		dbOperations.getBancaRepository().save(bpm);
		dbOperations.getBancaRepository().save(swissBank);
		dbOperations.getBancaRepository().save(ubs);
		dbOperations.getBancaRepository().save(bbva);
		dbOperations.getBancaRepository().save(creditAgricole);
		dbOperations.getBancaRepository().save(barclays);
		dbOperations.getBancaRepository().save(hsbc);
		// Clienti
		Cliente pinoRossi = new Cliente("Pino", "Rossi", 30, italia, null);
		Cliente annaRossi = new Cliente("Anna", "Rossi", 28, italia, pinoRossi);
		pinoRossi.setConiuge(annaRossi);
		Cliente johnDoe = new Cliente("John", "Doe", 45, inghilterra, null);
		Cliente susanDoe = new Cliente("Susan", "Doe", 40, inghilterra, johnDoe);
		johnDoe.setConiuge(susanDoe);
		Cliente miguelOrtiz = new Cliente("Miguel", "Ortiz", 20, spagna, null);
		Cliente augusteCheminais = new Cliente("Auguste", "Cheminais", 18, francia, null);
		Cliente xerdanShaqiri = new Cliente("Xerdan", "Shaqiri", 50, svizzera, null);
		dbOperations.getClienteRepository().save(pinoRossi);
		dbOperations.getClienteRepository().save(annaRossi);
		dbOperations.getClienteRepository().save(johnDoe);
		dbOperations.getClienteRepository().save(susanDoe);
		dbOperations.getClienteRepository().save(miguelOrtiz);
		dbOperations.getClienteRepository().save(augusteCheminais);
		dbOperations.getClienteRepository().save(xerdanShaqiri);
		// Conto Correnti
		ContoCorrente conto_1 = new ContoCorrente(1000, unicredit);
		ContoCorrente conto_2 = new ContoCorrente(100000, swissBank);
		ContoCorrente conto_3 = new ContoCorrente(5000, hsbc);
		ContoCorrente conto_4 = new ContoCorrente(8000, hsbc);
		ContoCorrente conto_5 = new ContoCorrente(300, creditAgricole);
		conto_1.addClienteToConto(pinoRossi);
		conto_1.addClienteToConto(annaRossi);
		conto_2.addClienteToConto(xerdanShaqiri);
		conto_3.addClienteToConto(johnDoe);
		conto_3.addClienteToConto(susanDoe);
		conto_4.addClienteToConto(miguelOrtiz);
		conto_4.addClienteToConto(xerdanShaqiri);
		conto_4.addClienteToConto(augusteCheminais);
		conto_5.addClienteToConto(augusteCheminais);
		dbOperations.getContoCorrenteRepository().save(conto_1);
		dbOperations.getContoCorrenteRepository().save(conto_2);
		dbOperations.getContoCorrenteRepository().save(conto_3);
		dbOperations.getContoCorrenteRepository().save(conto_4);
		dbOperations.getContoCorrenteRepository().save(conto_5);
		// Lavoratori
		Direttore francoBassi = new Direttore("Franco", "Bassi", 55, unicredit, 1);
		Direttore simoneFerri = new Direttore("Simone", "Ferri", 60, swissBank, 3);
		dbOperations.getDirettoreRepository().save(francoBassi);
		dbOperations.getDirettoreRepository().save(simoneFerri);
		Impiegato ginoDivani = new Impiegato("Gino", "Divani", 30, unicredit, 10);
		Impiegato davideGuerrini = new Impiegato("Davide", "Guerini", 30, creditAgricole, 10);
		Impiegato saraCaserti = new Impiegato("Sara", "Caserti", 30, bbva, 10);
		Impiegato valentinaPandolfi = new Impiegato("Valentini", "Pandolfi", 30, barclays, 10);
		dbOperations.getImpiegatoRepository().save(ginoDivani);
		dbOperations.getImpiegatoRepository().save(davideGuerrini);
		dbOperations.getImpiegatoRepository().save(saraCaserti);
		dbOperations.getImpiegatoRepository().save(valentinaPandolfi);
	}
 }
