package com.assignment.sistemabancario.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.assignment.sistemabancario.model.DBOperations;

@Controller
public class ManageDataViewController {
	
	@Autowired
	DBOperations dbOperations;

	@RequestMapping(value = "/manageNazione")
	public ModelAndView manageNazione() {
		ModelAndView mvManageNazione = new ModelAndView("manage_data_pages/manage_nazione");
		mvManageNazione.addObject("nazioni", dbOperations.getNazioneRepository().findAll());
		return mvManageNazione;
	}
	
	@RequestMapping(value = "/manageBanca")
	public ModelAndView manageBanca() {
		ModelAndView mvManageBanca = new ModelAndView("manage_data_pages/manage_banca");
		mvManageBanca.addObject("nazioni", dbOperations.getNazioneRepository().findAll());
		mvManageBanca.addObject("banche", dbOperations.getBancaRepository().findAll());
		return mvManageBanca;
	}
	
	@RequestMapping(value = "/manageCliente")
	public ModelAndView manageCliente() {
		ModelAndView mvManageCliente = new ModelAndView("manage_data_pages/manage_cliente");
		mvManageCliente.addObject("nazioni", dbOperations.getNazioneRepository().findAll());
		mvManageCliente.addObject("clienti", dbOperations.getClienteRepository().findAll());
		return mvManageCliente;
	}
	
	@RequestMapping(value = "/manageLavoratore")
	public ModelAndView manageLavoratore() {
		ModelAndView mvAddLavoratore = new ModelAndView("manage_data_pages/manage_lavoratore");
		mvAddLavoratore.addObject("banche", dbOperations.getBancaRepository().findAll());
		mvAddLavoratore.addObject("impiegati", dbOperations.getImpiegatoRepository().findAll());
		mvAddLavoratore.addObject("direttori", dbOperations.getDirettoreRepository().findAll());
		return mvAddLavoratore;
	}
	
	@RequestMapping(value = "/manageContoCorrente")
	public ModelAndView manageContoCorrente() {
		ModelAndView mvManageContoCorrente = new ModelAndView("manage_data_pages/manage_contoCorrente");
		mvManageContoCorrente.addObject("banche", dbOperations.getBancaRepository().findAll());
		mvManageContoCorrente.addObject("contiCorrenti", dbOperations.getContoCorrenteRepository().findAll());
		return mvManageContoCorrente;
	}
}
