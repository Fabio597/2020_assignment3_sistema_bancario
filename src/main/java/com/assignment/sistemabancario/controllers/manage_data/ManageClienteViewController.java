package com.assignment.sistemabancario.controllers.manage_data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Cliente;
import com.assignment.sistemabancario.model.db.Nazione;

@Controller
public class ManageClienteViewController {

	@Autowired
	DBOperations dbOperations;
	
	@RequestMapping(value = "/addClienteInDB", method = RequestMethod.POST)
	public ModelAndView addCliente(long nazioneCliente, long coniugeCliente, String nomeCliente, String cognomeCliente, int etaCliente) {
		Nazione nazione = null;
		Cliente coniuge = null;
		Cliente clienteDaAggiungere = new Cliente(nomeCliente, cognomeCliente, etaCliente);
		if (nazioneCliente != 0) {
			nazione = dbOperations.getNazioneRepository().findById(nazioneCliente).get();
		}
		if (coniugeCliente != 0) {
			coniuge = dbOperations.getClienteRepository().findById(coniugeCliente).get();
			coniuge.setConiuge(clienteDaAggiungere);
		}
		clienteDaAggiungere.setNazione(nazione);
		clienteDaAggiungere.setConiuge(coniuge);
		dbOperations.getClienteRepository().save(clienteDaAggiungere);
		ModelAndView mvAddCliente = new ModelAndView(new RedirectView("/manageCliente"));
		return mvAddCliente;
	}
	
	@RequestMapping(value = "/eliminaCliente", method = RequestMethod.POST)
	public ModelAndView eliminaCliente(long idCliente) {
		dbOperations.getClienteRepository().deleteById(idCliente);
		ModelAndView mvEliminaCliente = new ModelAndView(new RedirectView("/manageCliente"));
		return mvEliminaCliente;
	}
	
	@RequestMapping(value = "/modificaCliente", method = RequestMethod.POST)
	public ModelAndView modificaBanca(long idCliente) {
		Cliente clienteDaModificare = dbOperations.getClienteRepository().findById(idCliente).get();
		ModelAndView mvModificaCliente = new ModelAndView("manage_data_pages/modify_data_pages/modifica_cliente");
		mvModificaCliente.addObject("clienteDaModificare", clienteDaModificare);
		mvModificaCliente.addObject("nazioni", dbOperations.getNazioneRepository().findAll());
		mvModificaCliente.addObject("clienti", dbOperations.getClienteRepository().findAll());
		return mvModificaCliente;
	}
}
