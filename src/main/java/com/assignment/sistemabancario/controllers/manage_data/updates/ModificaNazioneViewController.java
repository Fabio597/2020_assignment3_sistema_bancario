package com.assignment.sistemabancario.controllers.manage_data.updates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Nazione;


@Controller
public class ModificaNazioneViewController {
	
	@Autowired
	DBOperations dbOperations;

	@RequestMapping(value = "/modificaNazioneInDB", method = RequestMethod.POST)
	public ModelAndView modificaNazione(long idNazione, String nomeNazione) {
		Nazione nazioneDaModificare = dbOperations.getNazioneRepository().findById(idNazione).get();
		nazioneDaModificare.setNome(nomeNazione);
		dbOperations.getNazioneRepository().save(nazioneDaModificare);
		ModelAndView modificaNazione = new ModelAndView(new RedirectView("/manageNazione"));
		return modificaNazione;
	}
}
