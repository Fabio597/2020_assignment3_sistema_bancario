package com.assignment.sistemabancario.controllers.manage_data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Banca;
import com.assignment.sistemabancario.model.db.Nazione;

@Controller
public class ManageBancaViewController {
	
	@Autowired
	DBOperations dbOperations;
	
	@RequestMapping(value = "/addBancaInDB", method = RequestMethod.POST)
	public ModelAndView addBanca(long nazioneBanca, String nomeBanca) {
		if (nazioneBanca != 0) {
			Nazione bancaNazione = dbOperations.getNazioneRepository().findById(nazioneBanca).get();
			Banca bancaDaAggiungere = new Banca(nomeBanca, bancaNazione);
			dbOperations.getBancaRepository().save(bancaDaAggiungere);
		}
		else {
			Banca bancaDaAggiungere = new Banca(nomeBanca, null);
			dbOperations.getBancaRepository().save(bancaDaAggiungere);
		}
		ModelAndView mvAddBanca = new ModelAndView(new RedirectView("/manageBanca"));
		return mvAddBanca;
	}
	
	@RequestMapping(value = "/eliminaBanca", method = RequestMethod.POST)
	public ModelAndView eliminaBanca(long idBanca) {
		dbOperations.getBancaRepository().deleteById(idBanca);
		ModelAndView mvEliminaBanca = new ModelAndView(new RedirectView("/manageBanca"));
		return mvEliminaBanca;
	}
	
	@RequestMapping(value = "/modificaBanca", method = RequestMethod.POST)
	public ModelAndView modificaBanca(long idBanca) {
		Banca bancaDaModificare = dbOperations.getBancaRepository().findById(idBanca).get();
		ModelAndView mvModificaBanca = new ModelAndView("manage_data_pages/modify_data_pages/modifica_banca");
		mvModificaBanca.addObject("bancaDaModificare", bancaDaModificare);
		mvModificaBanca.addObject("nazioni", dbOperations.getNazioneRepository().findAll());
		return mvModificaBanca;
	}
}
