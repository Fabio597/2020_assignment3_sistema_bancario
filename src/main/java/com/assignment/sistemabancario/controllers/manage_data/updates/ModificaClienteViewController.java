package com.assignment.sistemabancario.controllers.manage_data.updates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Cliente;
import com.assignment.sistemabancario.model.db.Nazione;

@Controller
public class ModificaClienteViewController {

	@Autowired
	DBOperations dbOperations;
	
	@RequestMapping(value = "/modificaClienteInDB", method = RequestMethod.POST)
	public ModelAndView modificaCliente(String nomeCliente, String cognomeCliente, int etaCliente, long nazioneCliente, long coniugeCliente, long idCliente) {
		Nazione nazione = null;
		Cliente coniuge = null;
		Cliente clienteDaModificare = dbOperations.getClienteRepository().findById(idCliente).get();
		if (nazioneCliente != 0)
			nazione = dbOperations.getNazioneRepository().findById(nazioneCliente).get();
		if (coniugeCliente != 0) {
			coniuge = dbOperations.getClienteRepository().findById(coniugeCliente).get();
			coniuge.setConiuge(clienteDaModificare);
		}
		clienteDaModificare.setNome(nomeCliente);
		clienteDaModificare.setCognome(cognomeCliente);
		clienteDaModificare.setEta(etaCliente);
		clienteDaModificare.setConiuge(coniuge);
		clienteDaModificare.setNazione(nazione);
		dbOperations.getClienteRepository().save(clienteDaModificare);
		ModelAndView mvModificaCliente = new ModelAndView(new RedirectView("/manageCliente"));
		return mvModificaCliente;
	}
}
