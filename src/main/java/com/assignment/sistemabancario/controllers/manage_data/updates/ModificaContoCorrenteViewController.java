package com.assignment.sistemabancario.controllers.manage_data.updates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Banca;
import com.assignment.sistemabancario.model.db.Cliente;
import com.assignment.sistemabancario.model.db.ContoCorrente;

@Controller
public class ModificaContoCorrenteViewController {

	@Autowired
	DBOperations dbOperations;
	
	@RequestMapping(value = "/addClienteToContoInDB", method = RequestMethod.POST)
	public ModelAndView addIntestatario(long idConto, long aggiungiIntestatario) {
		if (aggiungiIntestatario != 0) {
			ContoCorrente contoDaModificare = dbOperations.getContoCorrenteRepository().findById(idConto).get();
			Cliente intestatarioDaAggiungere = dbOperations.getClienteRepository().findById(aggiungiIntestatario).get();
			contoDaModificare.addClienteToConto(intestatarioDaAggiungere);
			dbOperations.getContoCorrenteRepository().save(contoDaModificare);
		}
		ModelAndView mvAddIntestatario = new ModelAndView(new RedirectView("/manageContoCorrente"));
		return mvAddIntestatario;
	}
	
	@RequestMapping(value = "/rimuoviIntestatario", method = RequestMethod.POST)
	public ModelAndView rimuoviIntestatario(long idConto, long idIntestatario) {
		Cliente intestatarioDaRimuovere = dbOperations.getClienteRepository().findById(idIntestatario).get();
		ContoCorrente contoDaModificare = dbOperations.getContoCorrenteRepository().findById(idConto).get();
		contoDaModificare.removeCliente(intestatarioDaRimuovere);
		dbOperations.getContoCorrenteRepository().save(contoDaModificare);
		ModelAndView mvRimuoviIntestatario = new ModelAndView(new RedirectView("/manageContoCorrente"));
		return mvRimuoviIntestatario;
	}
	
	@RequestMapping(value = "/modificaContoCorrenteInDB", method = RequestMethod.POST)
	public ModelAndView modificaContoCorrente(long idConto, long bancaConto, int saldo) {
		Banca bancaDaSettare = null;
		if (bancaConto != 0)
			bancaDaSettare = dbOperations.getBancaRepository().findById(bancaConto).get();
		ContoCorrente contoCorrenteDaModificare = dbOperations.getContoCorrenteRepository().findById(idConto).get();
		contoCorrenteDaModificare.setSaldo(saldo);
		contoCorrenteDaModificare.setBanca(bancaDaSettare);
		dbOperations.getContoCorrenteRepository().save(contoCorrenteDaModificare);
		ModelAndView mvModificaContoCorrente = new ModelAndView(new RedirectView("/manageContoCorrente"));
		return mvModificaContoCorrente;
	}
}
