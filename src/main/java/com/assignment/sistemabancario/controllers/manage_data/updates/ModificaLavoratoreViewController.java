package com.assignment.sistemabancario.controllers.manage_data.updates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Banca;
import com.assignment.sistemabancario.model.db.Direttore;
import com.assignment.sistemabancario.model.db.Impiegato;

@Controller
public class ModificaLavoratoreViewController {

	@Autowired
	DBOperations dbOperations;
	
	@RequestMapping(value = "/modificaLavoratoreInDB", method = RequestMethod.POST)
	public ModelAndView modificaLavoratore(long idLavoratore, String nomeLavoratore, String cognomeLavoratore, int etaLavoratore, long bancaLavoratore, int ufficioSportello, int tipoLavoratore) {
		Banca bancaDaSettare = dbOperations.getBancaRepository().findById(bancaLavoratore).get();
		if (tipoLavoratore==0) {
			Direttore direttoreDaModificare = dbOperations.getDirettoreRepository().findById(idLavoratore).get();
			direttoreDaModificare.setNome(nomeLavoratore);
			direttoreDaModificare.setCognome(cognomeLavoratore);
			direttoreDaModificare.setEta(etaLavoratore);
			direttoreDaModificare.setNumeroUfficio(ufficioSportello);
			direttoreDaModificare.setBanca(bancaDaSettare);
			dbOperations.getDirettoreRepository().save(direttoreDaModificare);
		} else {
			Impiegato impiegatoDaModificare = dbOperations.getImpiegatoRepository().findById(idLavoratore).get();
			impiegatoDaModificare.setNome(nomeLavoratore);
			impiegatoDaModificare.setCognome(cognomeLavoratore);
			impiegatoDaModificare.setEta(etaLavoratore);
			impiegatoDaModificare.setBanca(bancaDaSettare);
			impiegatoDaModificare.setNumeroSportello(ufficioSportello);
			impiegatoDaModificare.setBanca(bancaDaSettare);
			dbOperations.getImpiegatoRepository().save(impiegatoDaModificare);
		}
		ModelAndView mvModificaLavoratore = new ModelAndView(new RedirectView("/manageLavoratore"));
		return mvModificaLavoratore;
	}
}
