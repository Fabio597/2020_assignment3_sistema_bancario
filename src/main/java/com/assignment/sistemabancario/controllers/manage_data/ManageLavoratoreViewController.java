package com.assignment.sistemabancario.controllers.manage_data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Banca;
import com.assignment.sistemabancario.model.db.Direttore;
import com.assignment.sistemabancario.model.db.Impiegato;

@Controller
public class ManageLavoratoreViewController {

	@Autowired
	DBOperations dbOperations;
	
	@RequestMapping(value = "/addLavoratoreInDB", method = RequestMethod.POST)
	public ModelAndView addLavoratore(String nomeLavoratore, String cognomeLavoratore, int etaLavoratore, long bancaLavoratore, int tipoLavoratore, int ufficioSportello) {
		Banca banca = dbOperations.getBancaRepository().findById(bancaLavoratore).get();
		if (tipoLavoratore == 0) {
			// Direttore
			Direttore direttore = new Direttore(nomeLavoratore, cognomeLavoratore, etaLavoratore, banca, ufficioSportello);
			dbOperations.getDirettoreRepository().save(direttore);
		} else {
			// Impiegato
			Impiegato impiegato = new Impiegato(nomeLavoratore, cognomeLavoratore, etaLavoratore, banca, ufficioSportello);
			dbOperations.getImpiegatoRepository().save(impiegato);
		}
		ModelAndView mvAddLavoratore = new ModelAndView(new RedirectView("/manageLavoratore"));
		return mvAddLavoratore;
	}
	
	@RequestMapping(value = "/eliminaImpiegato", method = RequestMethod.POST)
	public ModelAndView eliminaImpiegato(long idImpiegato) {
		dbOperations.getImpiegatoRepository().deleteById(idImpiegato);
		ModelAndView mvEliminaImpiegato = new ModelAndView(new RedirectView("/manageLavoratore"));
		return mvEliminaImpiegato;
	}
	
	@RequestMapping(value = "/eliminaDirettore", method = RequestMethod.POST)
	public ModelAndView eliminaDirettore(long idDirettore) {
		dbOperations.getDirettoreRepository().deleteById(idDirettore);
		ModelAndView mvEliminaImpiegato = new ModelAndView(new RedirectView("/manageLavoratore"));
		return mvEliminaImpiegato;
	}
	
	@RequestMapping(value = "/modificaImpiegato", method = RequestMethod.POST)
	public ModelAndView modificaImpiegato(long idImpiegato) {
		Impiegato impiegatoDaModificare = dbOperations.getImpiegatoRepository().findById(idImpiegato).get();
		ModelAndView mvModificaImpiegato = new ModelAndView("manage_data_pages/modify_data_pages/modifica_lavoratore");
		mvModificaImpiegato.addObject("lavoratoreDaModificare", impiegatoDaModificare);
		mvModificaImpiegato.addObject("banche", dbOperations.getBancaRepository().findAll());
		mvModificaImpiegato.addObject("tipoLavoratore", 1);
		return mvModificaImpiegato;
	}
	
	@RequestMapping(value = "/modificaDirettore", method = RequestMethod.POST)
	public ModelAndView modificaDirettore(long idDirettore) {
		Direttore direttoreDaModificare = dbOperations.getDirettoreRepository().findById(idDirettore).get();
		ModelAndView mvModificaDirettore = new ModelAndView("manage_data_pages/modify_data_pages/modifica_lavoratore");
		mvModificaDirettore.addObject("lavoratoreDaModificare", direttoreDaModificare);
		mvModificaDirettore.addObject("banche", dbOperations.getBancaRepository().findAll());
		mvModificaDirettore.addObject("tipoLavoratore", 0);
		return mvModificaDirettore;
	}
}
