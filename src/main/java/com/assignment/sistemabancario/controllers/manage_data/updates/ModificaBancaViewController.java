package com.assignment.sistemabancario.controllers.manage_data.updates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Banca;

@Controller
public class ModificaBancaViewController {
	
	@Autowired
	DBOperations dbOperations;

	@RequestMapping(value = "/modificaBancaInDB", method = RequestMethod.POST)
	public ModelAndView modificaBanca(long idBanca, long nazioneBanca, String nomeBanca) {
		Banca bancaDaModificare = dbOperations.getBancaRepository().findById(idBanca).get();
		bancaDaModificare.setName(nomeBanca);
		if (nazioneBanca == 0) {
			bancaDaModificare.setNazione(null);
		} else {
			bancaDaModificare.setNazione(dbOperations.getNazioneRepository().findById(nazioneBanca).get());
		}
		dbOperations.getBancaRepository().save(bancaDaModificare);
		ModelAndView mvModificaBanca = new ModelAndView(new RedirectView("/manageBanca"));
		return mvModificaBanca;
	}
}
