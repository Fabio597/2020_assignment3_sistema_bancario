package com.assignment.sistemabancario.controllers.manage_data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Nazione;

@Controller
public class ManageNazioneViewController {

	@Autowired
	DBOperations dbOperations;
	
	@RequestMapping(value = "/addNazioneInDB", method=RequestMethod.POST)
	public ModelAndView addNazione(String nomeNazione) {
		Nazione nazioneToAdd = new Nazione(nomeNazione);
		dbOperations.getNazioneRepository().save(nazioneToAdd);
		ModelAndView mvAddNazione = new ModelAndView(new RedirectView("/manageNazione"));
		return mvAddNazione;
	}
	
	@RequestMapping(value = "/eliminaNazione", method=RequestMethod.POST)
	public ModelAndView eliminaNazione(long idNazione) {
		dbOperations.getNazioneRepository().deleteById(idNazione);
		ModelAndView mvEliminaNazione = new ModelAndView(new RedirectView("/manageNazione"));
		return mvEliminaNazione;
	}
	
	@RequestMapping(value = "/modificaNazione", method=RequestMethod.POST)
	public ModelAndView modificaNazione(long idNazione) {
		Nazione nazioneDaModificare = dbOperations.getNazioneRepository().findById(idNazione).get();
		ModelAndView modificaNazione = new ModelAndView("manage_data_pages/modify_data_pages/modifica_nazione");
		modificaNazione.addObject("nazioneDaModificare", nazioneDaModificare);
		return modificaNazione;
	}
}
