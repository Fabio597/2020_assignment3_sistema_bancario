package com.assignment.sistemabancario.controllers.manage_data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.assignment.sistemabancario.model.DBOperations;
import com.assignment.sistemabancario.model.db.Banca;
import com.assignment.sistemabancario.model.db.ContoCorrente;

@Controller
public class ManageContoCorrenteViewController {

	@Autowired
	DBOperations dbOperations;
	
	@RequestMapping(value = "/addContoCorrenteInDB", method = RequestMethod.POST)
	public ModelAndView addContoCorrente(long bancaContoCorrente, int saldo) {
		Banca bancaConto = null; 
		if (bancaContoCorrente != 0)
				bancaConto = dbOperations.getBancaRepository().findById(bancaContoCorrente).get();
		ContoCorrente contoCorrenteDaAggiungere = new ContoCorrente(saldo, bancaConto);
		dbOperations.getContoCorrenteRepository().save(contoCorrenteDaAggiungere);
		ModelAndView mvAddContoCorrente = new ModelAndView(new RedirectView("/manageContoCorrente"));
		return mvAddContoCorrente;
	}
	
	@RequestMapping(value = "/eliminaContoCorrente", method = RequestMethod.POST)
	public ModelAndView eliminaContoCorrente(long idConto) {
		dbOperations.getContoCorrenteRepository().deleteById(idConto);
		ModelAndView mvEliminaContoCorrente = new ModelAndView(new RedirectView("/manageContoCorrente"));
		return mvEliminaContoCorrente;
	}
	
	@RequestMapping(value = "/modificaContoCorrente", method = RequestMethod.POST)
	public ModelAndView modificaContoCorrente(long idConto) {
		ContoCorrente contoDaModificare = dbOperations.getContoCorrenteRepository().findById(idConto).get();
		ModelAndView mvModificaContoCorrente = new ModelAndView("manage_data_pages/modify_data_pages/modifica_contoCorrente");
		mvModificaContoCorrente.addObject("contoCorrenteDaModificare", contoDaModificare);
		mvModificaContoCorrente.addObject("banche", dbOperations.getBancaRepository().findAll());
		mvModificaContoCorrente.addObject("clienti", dbOperations.getClienteRepository().findAll());
		return mvModificaContoCorrente;
	}
}
